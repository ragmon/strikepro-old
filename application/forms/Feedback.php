<?php
class Application_Form_Feedback extends Zend_Form
{
	public function init()
    {
        // Вызываем родительский метод
        parent::init();
        
        $this->addElementPrefixPath('App_Validate', 'App/Validate/', 'validate');
        $this->addElementPrefixPath('App_Filter', 'App/Filter/', 'filter');
        $this->addElementPrefixPath('App_Form_Decorator', 'App/Form/Decorator/', 'decorator');
    }
	
    /**
     * 
     * Форма обратной связи
     */
    public function viewfeedback()
    {
        // Указываем action формы
        $helperUrl = new Zend_View_Helper_Url();
        $this->setAction('');
        
        // Указываем метод формы
        $this->setMethod('post');
        
        // Задаем атрибут class для формы
        $this->setAttrib('class', 'profile');
        
         
        // название компании
        $feedback_company_name = new Zend_Form_Element_Text('feedback_company_name', array(
            'required'    => false,
            'label'       => 'Название юридического лица:',
            'maxlength'   => '100',
            'validators'  => array(
                array('StringLength', true, array(1, 100))
             ),
            'filters'     => array('StringTrim'),
        ));
        $feedback_company_name->setAttrib('class', 'input_text');
        
        // вид деятельности компании
        $feedback_company_activity = new Zend_Form_Element_Text('feedback_company_activity', array(
            'required'    => false,
            'label'       => 'Вид деятельности:',
            'maxlength'   => '100',
            'validators'  => array(
                array('StringLength', true, array(1, 100))
             ),
            'filters'     => array('StringTrim'),
        ));
        $feedback_company_activity->setAttrib('class', 'input_text');
        
        // адрес компании
        $feedback_company_address = new Zend_Form_Element_Text('feedback_company_address', array(
            'required'    => false,
            'label'       => 'Город, Адрес:',
            'maxlength'   => '500',
            'validators'  => array(
                array('StringLength', true, array(1, 500))
             ),
            'filters'     => array('StringTrim'),
        ));
        $feedback_company_address->setAttrib('class', 'input_text');
        
        // телефон компании
        $feedback_company_phone = new Zend_Form_Element_Text('feedback_company_phone', array(
            'required'    => false,
            'label'       => 'Контактный телефон:',
            'maxlength'   => '100',
            'validators'  => array(
                array('StringLength', true, array(1, 100))
             ),
            'filters'     => array('StringTrim'),
        ));
        $feedback_company_phone->setAttrib('class', 'input_text');
        
        // Фио
        $feedback_fio = new Zend_Form_Element_Text('feedback_fio', array(
            'required'    => true,
            'label'       => 'ФИО:',
            'maxlength'   => '100',
            'validators'  => array(
                array('StringLength', true, array(1, 100))
             ),
            'filters'     => array('StringTrim'),
        ));
        $feedback_fio->setAttrib('class', 'input_text');
        
         // Должность
        $feedback_company_position = new Zend_Form_Element_Text('feedback_company_position', array(
            'required'    => false,
            'label'       => 'Должность:',
            'maxlength'   => '100',
            'validators'  => array(
                array('StringLength', true, array(1, 100))
             ),
            'filters'     => array('StringTrim'),
        ));
        $feedback_company_position->setAttrib('class', 'input_text');
        
        // адрес эл почты
        $feedback_email = new App_Form_Element_Email('feedback_email', array(
            'required'    => true,
        ));
        $feedback_email->setAttrib('class', 'input_text');
        
         // Сайт
        $feedback_company_siteurl = new Zend_Form_Element_Text('feedback_company_siteurl', array(
            'required'    => false,
            'label'       => 'Сайт:',
            'maxlength'   => '100',
            'validators'  => array(
                array('StringLength', true, array(1, 100))
             ),
            'filters'     => array('StringTrim'),
        ));
        $feedback_company_siteurl->setAttrib('class', 'input_text');
        
         // сообщение
        $feedback_message = new Zend_Form_Element_Textarea('feedback_message', array(
            'required'    => true,
            'label'       => 'Сообщение:',
            'maxlength'   => '100000',
            'validators'  => array(
                array('StringLength', true, array(1, 100000))
             ),
            'filters'     => array('StringTrim'),
        ));
        $feedback_message->setAttribs(array('cols' => '50', 'rows' => '10'));
        // Субмит
		$submit = new Zend_Form_Element_Submit('submit', array(
            'label'       => 'Отправить Сообщение',
		));
		
		//Добавление элементов в форму
		$this->addElements(array($feedback_company_name, $feedback_company_activity, $feedback_company_address,
								 $feedback_company_phone, $feedback_fio, $feedback_company_position, $feedback_email, $feedback_company_siteurl, $feedback_message, $submit));
        
		//Разбиваем элементы по группам
		$this->addDisplayGroup(	array(	'feedback_person_type', 'feedback_company_name', 'feedback_company_activity', 'feedback_company_address',
										'feedback_company_phone', 'feedback_fio', 'feedback_company_position', 'feedback_email', 'feedback_company_siteurl', 'feedback_message', 'submit'), 
 								'MarketDataGroup', 
 								array('legend' => '')
							);
	}
	
	public function viewotherfeedback()
    {
        // Указываем action формы
        $helperUrl = new Zend_View_Helper_Url();
        $this->setAction('');
        
        // Указываем метод формы
        $this->setMethod('post');
        
        // Задаем атрибут class для формы
        $this->setAttrib('class', 'profile');
        
        // Фио
        $feedback_fio = new Zend_Form_Element_Text('feedback_fio', array(
            'required'    => true,
            'label'       => 'ФИО:',
            'maxlength'   => '100',
            'validators'  => array(
                array('Alnum', true, array(true)),
                array('StringLength', true, array(1, 100))
             ),
            'filters'     => array('StringTrim'),
        ));
        $feedback_fio->setAttrib('class', 'input_text');
        
        // адрес эл почты
        $feedback_email = new App_Form_Element_Email('feedback_email', array(
            'required'    => true,
        ));
        $feedback_email->setAttrib('class', 'input_text');
        
         // сообщение
        $feedback_message = new Zend_Form_Element_Textarea('feedback_message', array(
            'required'    => true,
            'label'       => 'Сообщение:',
            'maxlength'   => '100000',
            'validators'  => array(
                array('Alnum', true, array(true)),
                array('StringLength', true, array(1, 100000))
             ),
            'filters'     => array('StringTrim'),
        ));
        $feedback_message->setAttribs(array('cols' => '50', 'rows' => '10'));
        
        // Субмит
		$submit = new Zend_Form_Element_Submit('submit', array(
            'label'       => 'Отправить Сообщение',
		));
		
		//Добавление элементов в форму
		$this->addElements(array($feedback_fio,$feedback_email, $feedback_message, $submit));
        
		//Разбиваем элементы по группам
		$this->addDisplayGroup(	array('feedback_fio', 'feedback_email', 'feedback_message', 'submit'), 
 								'MarketDataGroup', 
 								array('legend' => '')
							);
	}
	

/* the end */
}
