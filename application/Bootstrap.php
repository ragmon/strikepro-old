<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
	/**
	 *  Регистрация NameSpaces
	 */
	protected function _initRegisterNamespace()
	{
    	$autoloader = Zend_Loader_Autoloader::getInstance();
		$autoloader->registerNamespace('App_');
	}
	
	/**
	 *  Запуск сессий
	 */
	protected function _initSessions()
	{
		$config = new Zend_Config_Ini('../application/configs/sessions.ini', 'sandbox');
		require_once 'Zend/Session.php';
		Zend_Session::setOptions($config->toArray());
		Zend_Session::start();
	}
	
	/**
     * Настройка маршрутов
     */
    protected function _initRouter() 
    {
        // Подключение файла правил маршрутизации
        require_once ('../application/configs/routes.php');

        // Если переменная router не является объектом Zend_Controller_Router_Abstract, выбрасываем исключение
        if (!($router instanceof Zend_Controller_Router_Abstract)) {
            throw new Exception('Incorrect config file: routes');
        }
        
        return $router;
    }
    
}

