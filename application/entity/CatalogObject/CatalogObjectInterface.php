<?php
/**
 * Created by PhpStorm.
 * User: Aleksandr Taran
 * Date: 19.08.2016
 * Time: 14:04
 */

interface CatalogObjectInterface
{
    /**
     * CatalogObjectInterface constructor.
     * @param $Id
     * @return
     */
    public static function getInstance($Id);
}