<?php

require_once 'entity/CatalogObject/CatalogObjectFactory.php';

abstract class AbstractCatalogObjectFactory
{

    /**
    * Возвращает фабрику
    *
    * @return AbstractFactory - дочерний объект
    * @throws Exception
    */
    public static function getCatalogObjectFactory()
    {
        return new CatalogObjectFactory();
    }

    /**
    * Возвращает Объект каталога
    * @param $Id int
    * @return Product
    */
    abstract public function getCatalogObject($Id);
}