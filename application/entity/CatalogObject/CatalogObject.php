<?php
/**
 * Created by PhpStorm.
 * User: Aleksandr Taran
 * Date: 17.08.2016
 * Time: 17:10
 */

require_once 'entity/CatalogObject/CatalogObjectInterface.php';
require_once 'models/CatalogObjectModel.php';

class CatalogObject implements CatalogObjectInterface
{
    protected $_model;

    public $child_objects;
    protected $pattern;

    private $property = array(
        'cat_id',
        'cat_code',
        'cat_object_type',
    );

    /**
     * @var self
     */
    private static $instance;


    /**
     * Возвращает экземпляр себя
     *
     * @return self
     */
    public static function getInstance($Id, $id_type = null)
    {
        if (!(self::$instance instanceof self)) {
            self::$instance = new self($Id, $id_type);
        }
        return self::$instance;
    }

    /**
     * Конструктор закрыт
     */
    private function __construct($Id, $id_type = null)
    {
        $this->_model = new Application_Model_CatalogObject();

        if ($id_type == 'link') {
        $DbObject = $this->getCatalogObjectByLink($Id);
        }
        else {
            $DbObject = $this->getCatalogObject($Id);
        }

        if (!empty($DbObject)) {
            foreach ($DbObject as $property => $argument) {
                if(in_array($property, $this->property))
                    $this->{$property} = $argument;
            }
        }

        $this->setPattern('directory');
    }

    public function withChilds()
    {
        if($this->cat_object_type != 4)
        {
            $this->getChildObjects();
        }
        else {
            unset($this->child_objects);
        }

        return $this;
    }


    protected function getChildObjects()
    {
        $result = $this->_model->getChildObjects($this->cat_code);

        foreach ($result as $item) {
            $this->child_objects[] = AbstractCatalogObjectFactory::getCatalogObjectFactory()->getCatalogObject($item->cat_code);
        }
    }


    /**
     * Клонирование запрещено
     */
    private function __clone()
    {

    }

    /**
     * Сериализация запрещена
     */
    private function __sleep()
    {

    }

    /**
     * Десериализация запрещена
     */
    private function __wakeup()
    {

    }

    protected function getCatalogObject($Id)
    {
        return $this->_model->getCatalogObject($Id);
    }

    protected function getCatalogObjectByLInk($Id)
    {
        return $this->_model->getCatalogObjectByLink($Id);
    }

    protected function setPattern($pattern) {

        $this->pattern = $pattern;
    }

    public function getPattern()
    {
        return $this->pattern;
    }



    public function __debugInfo()
    {
        foreach ($this->property AS $key => $value)
        {
            $array[$value] = $this->{$value};
        }

        $array['child_objects'] = $this->child_objects;

        return $array;
    }
}