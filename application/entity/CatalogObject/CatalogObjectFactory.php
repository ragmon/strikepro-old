<?php
/**
 * Created by PhpStorm.
 * User: Aleksandr Taran
 * Date: 19.08.2016
 * Time: 14:04
 */

require_once 'entity/CatalogObject/CatalogObject.php';
require_once 'entity/CatalogObject/CatalogObjectTypeDirectory.php';
require_once 'entity/CatalogObject/CatalogObjectProductDirectory.php';
require_once 'models/CatalogObjectModel.php';


class CatalogObjectFactory extends AbstractCatalogObjectFactory
{
    private $_model;

    public function __construct()
    {
        $this->_model = new Application_Model_CatalogObject();
    }

    /**
     * @param int $Id
     * @return CatalogObject|CatalogObjectProductDirectory|CatalogObjectTypeDirectory
     * @throws Zend_Http_Exception
     */
    public function getCatalogObject($Id)
    {
        if($this->_model->RecordExits($Id)) {
            switch ($this->_model->getCatalogObject($Id)->cat_object_type) {
                case 1:
                    return CatalogObjectTypeDirectory::getInstance($Id);
                    break;
                case 3:
                    return CatalogObjectTypeDirectory::getInstance($Id);
                    break;
                case 4:
                    return CatalogObjectProductDirectory::getInstance($Id);
                    break;
            }
        }
        else {
            throw new Zend_Http_Exception('Запрашиваемый объект не найден', 404);
        }
    }

    public function getCatalogObjectByLink($Id)
    {
        if($this->_model->RecordExitsByLink($Id)) {

            switch ($this->_model->getCatalogObjectByLink($Id)->cat_object_type) {
                case 1:
                    return CatalogObjectTypeDirectory::getInstance($Id, 'link');
                    break;
                case 3:
                    return CatalogObjectTypeDirectory::getInstance($Id, 'link');
                    break;
                case 4:
                    return CatalogObjectProductDirectory::getInstance($Id, 'link');
                    break;
            }
        }
        else {
            throw new Zend_Http_Exception('Запрашиваемый объект не найден', 404);
        }
    }
}