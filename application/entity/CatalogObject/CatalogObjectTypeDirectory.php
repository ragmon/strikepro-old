<?php
/**
 * Created by PhpStorm.
 * User: Aleksandr Taran
 * Date: 20.08.2016
 * Time: 16:36
 */
require_once 'entity/CatalogObject/CatalogObject.php';

class CatalogObjectTypeDirectory extends CatalogObject implements CatalogObjectInterface
{

    private $property = array(
        'cat_id',
        'cat_code',
        'cat_object_type',
        'cat_sub_of',
        'cat_position',
        'cat_refactor',
        'cat_name',
        'cat_link',
        'cat_image',
        'cat_image_small',
        'cat_description',
        'cat_show_on_site',
    );

    public $images;


    private static $instance;


    public static function getInstance($Id, $id_type = null)
    {
        self::$instance = new self($Id, $id_type);
        return self::$instance;
    }

    /**
     * Конструктор закрыт
     */
    private function __construct($Id, $id_type)
    {
        $this->_model = new Application_Model_CatalogObject();

        if ($id_type == 'link') {
            $DbObject = $this->getCatalogObjectByLink($Id);
        }
        else {
            $DbObject = $this->getCatalogObject($Id);
        }

        if (!empty($DbObject)) {
            foreach ($DbObject as $property => $argument) {
                if(in_array($property, $this->property))
                    $this->{$property} = $argument;
            }
        }

        $this->setPattern('directory');

        $this->images = ObjectImagesFactory::getInstance($this->cat_code)->get();
    }

    /**
     * Клонирование запрещено
     */
    private function __clone()
    {
    }

    /**
     * Сериализация запрещена
     */
    private function __sleep()
    {
    }

    /**
     * Десериализация запрещена
     */
    private function __wakeup()
    {

    }


    public function __debugInfo()
    {
        foreach ($this->property AS $key => $value)
        {
            $array[$value] = $this->{$value};
        }

        $array['child_objects'] = $this->child_objects;
        $array['pattern'] = $this->getPattern();
        return $array;
    }
}