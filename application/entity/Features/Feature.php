<?php
/**
 * Created by PhpStorm.
 * User: Aleksandr Taran
 * Date: 21.08.2016
 * Time: 17:04
 */
require_once 'models/FeaturesModel.php';

class Feature
{
    protected $_model;

    private $property = array(
        'product_types_id',
        'product_code',
        'features_id',
        'order',
        'title',
        'measurement',
        'description',
        'value'
    );

    public function __construct($item)
    {
        $this->_model = new Application_Model_Features();
        if (!empty($item)) {
            foreach ($item as $property => $argument) {
                if(in_array($property, $this->property))
                    $this->{$property} = $argument;
            }
        }
    }

    public function save()
    {
        $data['product_code'] = $this->product_code;
        $data['features_id'] = $this->features_id;
        $data['value'] = $this->value;

        $this->_model->save($data);
    }

    public function __debugInfo()
    {
        foreach ($this->property AS $key => $value)
        {
            $array[$value] = $this->{$value};
        }

        return $array;
    }
}