<?php
/**
 * Created by PhpStorm.
 * User: Aleksandr Taran
 * Date: 23.08.2016
 * Time: 17:00
 */

require_once 'entity/ObjectImages/ObjectImage.php';
require_once 'models/ObjectImagesModel.php';


class ObjectImagesFactory
{
    private static $instance;
    private $_Id;

    protected $_model;
    protected $images = array();

    private function __construct($Id)
    {
        $this->_model = new Application_Model_ObjectImages();
        $this->_parent = $Id;
        foreach ($this->_model->getObjectImages($this->_parent) AS $item){
            $this->pushObjectImages(new ObjectImage($item));
        }
    }

    private function pushObjectImages(ObjectImage $ObjectImage)
    {
        $this->images[] = $ObjectImage;
    }

    public static function getInstance($Id)
    {
        self::$instance = new self($Id);
        return self::$instance;
    }

    public function get()
    {
        return $this->images;
    }

    public function addImage($data)
    {
        $this->_model->addImage($data);
        $this->pushObjectImages(new ObjectImage($data));

        return $this;
    }

}