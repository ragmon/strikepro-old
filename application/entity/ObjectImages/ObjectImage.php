<?php
/**
 * Created by PhpStorm.
 * User: Aleksandr Taran
 * Date: 21.08.2016
 * Time: 17:04
 */
require_once 'models/ObjectImagesModel.php';

class ObjectImage
{
    protected $_model;
    protected $_table = 'catalog_images';

    private $property = array(
        'Id',
        'cat_code',
        'primary',
        'file',
        'alt'
    );

    public function __construct($item)
    {
        if (!empty($item)) {
            foreach ($item as $property => $argument) {
                if(in_array($property, $this->property)) {
                    $this->{$property} = $argument;
                }
            }
        }
    }

    public static function delete($Id)
    {
        return Application_Model_ObjectImages::getDefaultAdapter()->delete('catalog_images', array('Id = ?' => $Id));
    }

    public static function get($Id)
    {
        return Application_Model_ObjectImages::getDefaultAdapter()->fetchRow(
            Application_Model_ObjectImages::getDefaultAdapter()->select()->from('catalog_images')->where('Id = ?', $Id));
    }

    public static function update($Id, $data)
    {
        return Application_Model_ObjectImages::getDefaultAdapter()->update('catalog_images', $data, array('Id = ?' => $Id));
    }

    public function __debugInfo()
    {
        foreach ($this->property AS $key => $value) {
            $array[$value] = $this->{$value};
        }
        return $array;
    }
}