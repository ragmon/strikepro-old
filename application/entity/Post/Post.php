<?php
/**
 * Created by PhpStorm.
 * User: Aleksandr Taran
 * Date: 28.08.2016
 * Time: 17:19
 */

require_once 'models/PostModel.php';

class Application_Entity_Post_Post
{
    private $_model;

    private $_fields = array(
        'Id',
        'title',
        'description',
        'link',
        'headimage',
        'body',
        'posted_at',
    );

    public function __construct($data = null)
    {
        $this->_model = new Application_Model_PostModel();
        if($data){
            $this->set($data);
        }
        else {
            foreach ($this->_fields AS $field => $argument) {
                $this->{$field} = null;
            }
        }
    }

    public function find($id)
    {
        if($result = $this->_model->get($id)) {

            $this->set($result);
            return $this;
        }
        else {
            throw new Zend_Controller_Action_Exception('Не найден указанный объект', 404);
        }
    }

    public function all()
    {
        return $this->_model->all();
    }

    public function get()
    {
        return $this;
    }

    public function first()
    {
        $this->set($this->_model->first());
        return $this;
    }

    public function set($data)
    {
        foreach ($data AS $field => $argument) {
            if(in_array($field, $this->_fields))
                $this->{$field} = $argument;
        }

        $this->executeMutators();
    }

    public function save($data = null)
    {
        if(is_array($data)) {
            $this->set($data);
        }

        $this->executePrepare();

        if(is_null($this->Id)) {
            $this->_model->insert($this->getArray());
        }
        else {
            $this->_model->update($this->getArray(), array('Id = ?' => $this->Id));
        }
    }

    public function getArray()
    {
        foreach($this->_fields AS $field => $argument) {
            if(property_exists($this, $field)) {
                $data["{$argument}"] = $this->{$argument};
            }
        }

        return $data;
    }

    protected function lastScope($count = null)
    {
        $this->_model->order('posted_at', 'DESC');

        if($count !== null) {
            $this->_model->limit($count);
        }
    }

    protected function postedScope()
    {
        $d = new DateTime('now');
        $this->where(['posted_at', '<=', $d->format('Y-m-d H:i:s')]);
    }

    protected function unpostedScope()
    {
        $d = new DateTime('now');
        $this->where(['posted_at', '>=', $d->format('Y-m-d H:i:s')]);
    }

    public function PostedAtMutator()
    {
        $this->posted_at = new DateTime($this->posted_at);
    }

    public function PostedAtPrepare()
    {
        $this->posted_at = $this->posted_at->format('Y-m-d H:i:s');
    }

    public function destroy($Id)
    {
        return $this->_model->destroy($Id);
    }


    public function __call($name, $arguments)
    {
        if(method_exists($this, $name . 'Scope')) {
            $name = $name . 'Scope';

            if(count($arguments)) {
                $this->$name($arguments[0]);
            }
            else {
                $this->{$name}();
            }

            return $this;
        }
    }

    public function executeMutators()
    {
        $mutatorsMethods = get_class_methods( get_class($this) );
        foreach($mutatorsMethods AS $method) {
            if($method == 'executeMutators') continue;
            if(stripos($method, 'Mutator') ) {
                $this->{$method}();
            }
        }
    }

    public function executePrepare()
    {
        $PrepareMethods = get_class_methods( get_class($this) );
        foreach($PrepareMethods AS $method) {
            if($method == 'executePrepare') continue;
            if(stripos($method, 'Prepare') ) {
                $this->{$method}();
            }
        }
    }

    public function where($where)
    {
        $this->_model->where($where);
        return $this;
    }
}