<?php

class Application_Entity_FileManager_File
{
    private $file;

    public function __construct($file = null)
    {
        $this->file = $file;
    }

    public function get()
    {
        return $this->file;
    }

}