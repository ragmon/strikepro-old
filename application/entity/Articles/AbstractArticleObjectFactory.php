<?php

require_once 'entity/CatalogObject/ArticleObjectFactory.php';

abstract class AbstractArticleObjectFactory
{

    /**
    * Возвращает фабрику
    *
    * @return AbstractFactory - дочерний объект
    * @throws Exception
    */
    public static function getArticleObjectFactory()
    {
        return new ArticleObjectFactory();
    }

    /**
    * Возвращает Объект каталога
    * @param $Id int
    * @return Product
    */
    abstract public function getCatalogObject($Id);
}