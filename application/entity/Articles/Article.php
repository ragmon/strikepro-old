<?php
/**
 * Created by PhpStorm.
 * User: Aleksandr Taran
 * Date: 28.08.2016
 * Time: 17:19
 */

require_once 'models/ArticleModel.php';

class Article
{
    protected $_model;

    private $property = array(
        'article_art',
        'article_cat',
        'art_insale',
        'art_price',
        'article_name',
        'article_code',
        'article_color',
        'article_image',
        'article_image_big',
    );

    public function __construct($item)
    {
        $this->_model = new Application_Model_Features();
        if (!empty($item)) {
            foreach ($item as $property => $argument) {
                if(in_array($property, $this->property))
                    $this->{$property} = $argument;
            }
        }
        $config = new Zend_Config_Ini('configs/application.ini', 'production');

        $this->article_image_old = $this->article_image;

        $this->article_image = $config->products->directory->thumbnail
            . '/'
            . $this->article_color
            . $config->products->directory->thumbnail_ext;

        $this->article_image_big = $config->products->directory->colors
            . '/'
            . $this->article_color
            . $config->products->directory->colors_ext;
    }

    public function __debugInfo()
    {
        foreach ($this->property AS $key => $value)
        {
            $array[$value] = $this->{$value};
        }

        return $array;
    }
}