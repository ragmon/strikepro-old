$(function () {
    var container = $('.masonry-container');
    container.imagesLoaded( function () {
        container.masonry({
            columnWidth: '.item',
            itemSelector: '.item',
            percentPosition: true
        });
    });
});

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

$(document).ready(function() {

    $('#file-box').on('click', '.selectFileButton', function(event) {
        event.preventDefault();
        var funcNum = getUrlParameter('CKEditorFuncNum');
        var fileUrl = $(this).parent().parent().parent().children('.file').attr('src');
        if(funcNum) {
            window.opener.CKEDITOR.tools.callFunction(funcNum, fileUrl);
            window.close();
        }
        else {
            var element = getUrlParameter('element');
            window.opener.callbackFileManager(element, $(this).parent().parent().parent().children('.file').attr('src'));
            window.close();
        }
    });

    $('#file-box').on('click', '.deleteFileButton', function(event) {
        event.preventDefault();
        $('#deleteButton').attr('data-file', $(this).attr('data-file'));
        $('#deleteFilesModal').modal('show');
    });


    $('#deleteButton').on('click', function() {
        var file = $(this).attr('data-file');
        var url = '<?php echo $this->baseUrl("files/delete"); ?>' + '/' + file;

        $.ajax({
            type: 'GET',
            url: url,
            dataType: 'json',
            success: function (data) {
                if(data.success == true) {
                    $("div[data-key='"+ data.file +"'").remove();
                    var container = $('.masonry-container');

                    $(container).masonry('reloadItems');
                    $(container).masonry('layout');
                    $('#deleteFilesModal').modal('hide');
                }
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });

    $('#addFileButton').on('click', function(event){
        event.preventDefault();
        $('#addFilesModal').modal('show');
    });

    $(function() {
        var uploader = new ss.SimpleUpload({
            button: 'browseButton',
            dropzone: 'dropbox',
            url: '<?php echo $this->baseUrl("files/store"); ?>',
            progressUrl: '<?php echo $this->baseUrl("files/progress"); ?>',
            responseType: 'json',
            name: 'uploadfile',
            multiple: true,
            allowedExtensions: ['jpg', 'jpeg', 'png', 'gif'],
            hoverClass: 'ui-state-hover',
            focusClass: 'ui-state-focus',
            disabledClass: 'ui-state-disabled',
            onSubmit: function(filename, extension) {
                var progress = document.createElement('div'),
                    bar = document.createElement('div'),
                    fileSize = document.createElement('div'),
                    wrapper = document.createElement('div'),
                    progressBox = document.getElementById('progressBox');

                progress.className = 'progress progress-striped';
                bar.className = 'progress-bar progress-bar-success';
                fileSize.className = 'size';
                wrapper.className = 'wrapper';

                progress.appendChild(bar);
                wrapper.innerHTML = '<div class="name">'+filename+'</div>'; // filename is passed to onSubmit()
                wrapper.appendChild(fileSize);
                wrapper.appendChild(progress);
                progressBox.appendChild(wrapper);

                this.setProgressBar(bar);
                this.setFileSizeBox(fileSize);
                this.setProgressContainer(wrapper);
            },
            onComplete:   function(filename, response) {
                if (!response) {
                    alert(filename + 'upload failed');
                    return false;
                }
                console.log(response);
                console.log(response.output.substring(8));
                var caption = document.createElement('div');
                caption.className = 'caption';
                caption.innerHTML = '<p><a href="javascript:void(0);" class="btn btn-success selectFileButton" role="button">выбрать</a>' +
                    '<a href="javascript:void(0);" data-file="'+ response.output.substring(8) +'" class="btn btn-sm btn-danger deleteFileButton" role="button">удалить</a>' +
                    '</p>';

                var file = document.createElement('img');
                file.className = 'file img-responsive img-thumbnail';
                file.setAttribute('src', '<?php echo $this->baseUrl(); ?>' + response.output);

                var thumbnail = document.createElement('div');
                thumbnail.className = 'thumbnail file-container';

                thumbnail.appendChild(file);
                thumbnail.appendChild(caption);

                var cell = document.createElement('div');
                cell.className = 'col-xs-4 col-sm-2 item';
                cell.setAttribute('data-key', response.output.substring(8));
                cell.appendChild(thumbnail);

                var fileBox = document.getElementById('file-box');
                fileBox.appendChild(cell);

                var container = $('.masonry-container');

                $(container).masonry('reloadItems');
                $(container).masonry('layout');

                $('#addFilesModal').modal('hide');
            }
        });
    });

});