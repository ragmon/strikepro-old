<?php
/**
 * Created by PhpStorm.
 * User: Aleksandr Taran
 * Date: 24.08.2016
 * Time: 12:56
 */

require_once 'entity/CatalogObject/AbstractCatalogObjectFactory.php';

class STPR_View_Helper_GetObjectImage extends Zend_View_Helper_Abstract
{
    private $output = null;

    public function GetObjectImage($code)
    {
        $this->catalog_object
            = AbstractCatalogObjectFactory::getCatalogObjectFactory()->getCatalogObject(intval($code));

        foreach($this->catalog_object->images AS $image) {
            $this->output .=
            "<div id=\"image_id_$image->Id\" class=\"col-sm-6 col-md-4\">
                <div class=\"thumbnail\">
                    <div class=\"dropdown\" style=\"position:absolute; right:30px; top:10px;\">";

                        if(!is_null($image->primary)) {
                            $this->output .= "<span class=\"btn disable\" title=\"Основная\"><i style=\"color:rgb(123, 181, 48)\" class=\"fa fa-check-square fa-2x\" aria-hidden=\"true\"></i></span>";
                        }

            $this->output .= "<button class=\"btn dropdown-toggle\" type=\"button\" id=\"imageAction\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"true\">
                            <i class=\"fa fa-bars\" aria-hidden=\"true\"></i>
                            </button>
                   
                        <ul class=\"dropdown-menu\" aria-labelledby=\"imageAction\">
                            <li><a class=\"primary-image-button\"href=\"#\" >Назначить основной</a></li>
                            <li><a class=\"edit-image-button\" data-id=\"$image->Id\" data-toggle=\"modal\" data-target=\"#editModal\" href=\"#\">Редактировать описание</a></li>
                            <li><a class=\"delete-image-button\" href=\"#\" data-id=\"$image->Id\" data-toggle=\"modal\" data-target=\"#deleteModal\" >Удалить</a></li>
                        </ul>
                    </div>
                    
                    <img style=\"max-height:180px;\" class=\"img-responsive\" src=\"$image->file\" alt=\"$image->alt\">
                    <div class=\"caption\">
                        <p>$image->alt</p>
                    </div>
                </div>
            </div>\n";
        }
        return $this->output;
    }
}



