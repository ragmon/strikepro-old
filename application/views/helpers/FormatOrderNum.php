<?php
//@author WSS http://websitespb.ru/, ffl.public@gmail.com
    class STPR_View_Helper_FormatOrderNum extends Zend_View_Helper_Abstract
    {
      
        protected $_last_error = false;
        
        public function FormatOrderNum($onum){
            $fmtnum = 5;//total digits
            return sprintf('%0'.$fmtnum.'s', "$onum");
        }
        
    }
