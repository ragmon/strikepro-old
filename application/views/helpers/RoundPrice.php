<?php
//@author WSS http://websitespb.ru/, ffl.public@gmail.com
    class STPR_View_Helper_RoundPrice extends Zend_View_Helper_Abstract
    {
      
        protected $_last_error = false;
        
        public function RoundPrice($price){
            return round($price, 2);
        }
        
    }
