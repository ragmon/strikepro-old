<?php
//@author WSS http://websitespb.ru/, ffl.public@gmail.com
    class STPR_View_Helper_JsEscape extends Zend_View_Helper_Abstract
    {
      
        protected $_last_error = false;
        
        function JsEscape($string){
            return str_replace("\n", '\n', str_replace('"', '\"', addcslashes(str_replace("\r", '', (string)$string), "\0..\37'\\")));
        }
        
    }
