<?php

class STPR_View_Helper_CountItem extends Zend_View_Helper_Abstract {
      
        protected $_last_error = false;
        protected $_cartModel = false;
        
        public function CountItem($mode = false, $dlvr_mode_idx = false) {
            
        	// создаем массив переменную, содержащую информацию из файла конфигурации
            $delivery_cfg = new Zend_Config_Ini('../application/configs/strikepro.ini', 'delivery');
			
            $dlv_idx = $deliverycosts = 0;
            
            if(!intval($dlvr_mode_idx) && array_key_exists('dlvr_mode_idx', $_REQUEST)){
                
            	$dlvr_mode_idx = intval($_REQUEST['dlvr_mode_idx']);
            }
            
            if(!intval($dlvr_mode_idx)){
            	
            	$dlvr_mode_idx = 1;
            }
            
            if( intval($dlvr_mode_idx) > count($delivery_cfg->deliverynames) ){
            	
            	$dlvr_mode_idx = 1;
            }
            
            foreach( $delivery_cfg->deliverynames as $dlv_name ){
                
            	$dlv_idx++;
            	
                if($dlvr_mode_idx == $dlv_idx){
                    
                	$deliverycosts = $delivery_cfg->deliveryprices->$dlv_idx;
                }
            }
            
            
            $cart_html = '<!-- application/views/helpers/DisplayCart.php -->';
            
            if(!$this->_cartModel){
                
            	$this->_cartModel = new Application_Model_CartModel();
            }
            
            $cart_products = $this->_cartModel->getCartProducts();
            
            $cart_total = 0;
            $cart_count = 0;
            
            foreach ($cart_products  as $productrow) {
                
            	$cart_count += $productrow['product_count'];
                $cart_total += round($productrow['product_count'] * round($productrow['art_price'], 2), 2);
            }
            if($cart_total > $delivery_cfg->freedeliveryamount) {
                
            	$total_w_delivery = $cart_total;
                $deliverycosts = 0; 	//means free
            }
            else{
                
            	$total_w_delivery = ( $cart_total + $deliverycosts );
            }
            
            
            if( $mode && $mode=='raw' ){
                
            	return array(
                    'countarts' 			=> count($cart_products),
                    'countitems' 			=> $cart_count,
                    'total' 				=> $cart_total,
                    'total_w_delivery' 		=> ($total_w_delivery),
                    'total_fmt' 			=> sprintf('%.2f', $cart_total),
                    'total_fmt_w_delivery' 	=> sprintf('%.2f', $total_w_delivery),
                    'deliverycosts' 		=> $deliverycosts,
                    'deliverycosts_fmt' 	=>  sprintf('%.2f', $deliverycosts),
                );
            }
            else{
                return $cart_count;
            }
        }
        
    }

