<?php
/**
 * Created by PhpStorm.
 * User: Aleksandr Taran
 * Date: 21.02.2016
 * Time: 21:29
 */
class STPR_View_Helper_FormatToTen extends Zend_View_Helper_Abstract
{

    protected $_last_error = false;

    public function FormatToTen($sum)
    {
        $sum = round($sum, 0);
        $newSum = round($sum, -1);
        if($newSum > $sum) {
            $newSum = $newSum - 10;
        }

        return $newSum;
    }

}