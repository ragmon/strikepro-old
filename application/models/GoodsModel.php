<?php
/**
 * Created by PhpStorm.
 * User: Aleksandr Taran
 * Date: 28.08.2016
 * Time: 17:30
 */

require_once 'Zend/Db/Table/Abstract.php';

class Application_Model_Goods extends Zend_Db_Table_Abstract
{
    public $_link;

    public function __construct()
    {
        parent::__construct();

        $this->_name = 'catalog_articles';
        $this->_link = 'article_link';
        $this->_cat = 'article_cat';
    }

    public function getGoodsByLink($link)
    {
        $select = $this->getAdapter()->select()
            ->from($this->_name)
            ->where($this->getAdapter()->quoteInto($this->_link . '=?', $link));

        $stmt = $this->getAdapter()->query($select);
        $result = $stmt->fetch();

        return $result;
    }

    public function getGoodsListByCat($catId)
    {
        $select = $this->getAdapter()->select()
            ->from($this->_name)
            ->where($this->getAdapter()->quoteInto($this->_cat . '=?', $catId));

        $stmt = $this->getAdapter()->query($select);
        $result = $stmt->fetchAll(Zend_Db::FETCH_OBJ);

        return $result;
    }

    public function getPath($parentCode) {

        $this->_name = 'catalog_cats';
        $where = $this->getAdapter()->quoteInto('cat_code = ?', $parentCode);

        $select = $this->getAdapter()->select()
            ->from($this->_name)
            ->where($where);

        $stmt = $this->getAdapter()->query($select);

        // Получение данных
        $result = $stmt->fetch();

        if(is_null($result['cat_sub_of'])) {
            $this->array[] = $result;
            return array_reverse($this->array);
        }
        else
        {
            $this->array[] = $result;
            return $this->getPath($result['cat_sub_of']);
        }
    }
}