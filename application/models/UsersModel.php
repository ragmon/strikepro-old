<?php
/**
 * UsersModel
 * 
 * @author alexbolin
 * @version 
 */
require_once 'Zend/Db/Table/Abstract.php';
class Application_Model_UsersModel extends Zend_Db_Table_Abstract
{
    /**
     * The default table name 
     */
    protected $_name = 'users';
    
	/**
	 * Insert new User in DataBase
	 * @param array $data
	 */
    public function addUser($data)
    {
    	 
    	if(!empty($data['user_password'])) {
    		
    		$data['user_password'] = SHA1($data['user_password']);
    	}
    	
    	$this->getAdapter()->insert($this->_name, $data);
    	
    	$result = $this->getAdapter()->lastInsertId();
    	
    	return $result;
    }

}

