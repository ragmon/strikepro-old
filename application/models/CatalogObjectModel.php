<?php

require_once 'Zend/Db/Table/Abstract.php';

class Application_Model_CatalogObject extends Zend_Db_Table_Abstract
{
    public $_ID_;

    public function __construct()
    {
        parent::__construct();
        $this->_name = 'catalog_cats';
        $this->_ID_ = 'cat_code';
    }

    public function RecordExits($Id)
    {
        $where = $this->getAdapter()->quoteInto($this->_ID_ . ' = ?', $Id);

        $query = $this->getAdapter()->select()
                ->from($this->_name, array("num"=>"COUNT(*)"))
                ->where($where);

        $stmt = $this->getAdapter()->query($query);
        $result = $stmt->fetchObject();

        return $result;
    }

    public function RecordExitsByLink($Id)
    {
        $this->_ID_ = 'cat_link';

        $where = $this->getAdapter()->quoteInto($this->_ID_ . ' = ?', $Id);

        $query = $this->getAdapter()->select()
            ->from($this->_name, array("num"=>"COUNT(*)"))
            ->where($where);

        $stmt = $this->getAdapter()->query($query);
        $result = $stmt->fetchObject();

        return $result;
    }

    public function getCatalogObject($Id)
    {
        $where = $this->getAdapter()->quoteInto($this->_ID_ . ' = ?', $Id);

        return $this->getAdapter()->query($this->getAdapter()->select()
                ->from($this->_name)
                ->where($where))->fetchObject();
    }

    public function getCatalogObjectByLink($Id)
    {
        $this->_ID_ = 'cat_link';

        $where = $this->getAdapter()->quoteInto($this->_ID_ . ' = ?', $Id);

        return $this->getAdapter()->query($this->getAdapter()->select()
                                            ->from($this->_name)
                                            ->where($where))->fetchObject();
    }

    public function getChildObjects($Id)
    {
        $where = $this->getAdapter()->quoteInto('cat_sub_of = ?', $Id);
        $where2 = $this->getAdapter()->quoteInto('cat_show_on_site = ?', 1);

        $stmt = $this->getAdapter()->query($this->getAdapter()->select()
                                                                ->from($this->_name)
                                                                ->where($where)
                                                                ->where($where2)
                                                                ->order('cat_position', 'ASC')
                                            );
        while($row = $stmt->fetchObject())
        {
            $result[] = $row;
        }
        $stmt->closeCursor();

        return $result;
    }

    public function getPath($parentCode) {

        $this->_name = 'catalog_cats';
        $where = $this->getAdapter()->quoteInto('cat_code = ?', $parentCode);

        $select = $this->getAdapter()->select()
            ->from($this->_name)
            ->where($where);

        $stmt = $this->getAdapter()->query($select);

        // Получение данных
        $result = $stmt->fetch();

        if(is_null($result['cat_sub_of'])) {
            $this->array[] = $result;
            return array_reverse($this->array);
        }
        else
        {
            $this->array[] = $result;
            return $this->getPath($result['cat_sub_of']);
        }
    }


    public function save($data)
    {
        $where = $this->getAdapter()->quoteInto('cat_code = ?', intval($data['cat_code']));

        try
        {
            $response = $this->getAdapter()->update('catalog_cats', $data, $where);
        }
        catch (Zend_Db_Exception $e)
        {
            die('Something went wrong: ' . $e->getMessage());
        }
    }

    public function insertAllFeatures()
    {
        $cat_type = 1;
        $where = $this->getAdapter()->quoteInto('cat_type = ?', $cat_type);

        $select = $this->getAdapter()->select()
            ->from($this->_name)
            ->where($where);

        $stmt = $this->getAdapter()->query($select);

        // Получение данных
        $cats = $stmt->fetchAll();


        $where = $this->getAdapter()->quoteInto('product_types_id = ?', $cat_type);


        $select = $this->getAdapter()->select()
            ->from('type_features')
            ->where($where);

        $stmt = $this->getAdapter()->query($select);

        // Получение данных
        $features = $stmt->fetchAll();

        foreach($cats AS $cat)
        {
            foreach($features AS $feature)
            {
                $this->getAdapter()->insert('product_features_value',
                    array('product_code' => $cat['cat_code'], 'features_id' => $feature['features_id'] ));
            }
        }
    }
}
