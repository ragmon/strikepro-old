<?php

/**
 * FeedbackModel
 * 
 * @author alexbolin
 * @version 
 */

require_once 'Zend/Db/Table/Abstract.php';

class Application_Model_FeedbackModel extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'feedback_1';
	protected $feed;
	
	public function exchange_array($data, $department_id)
	{
		
		$department = $this->getDepartment((int)$department_id);
		
		if( isset($department['id']) ) {
			$this->feed['department_id'] = $department['id'];
		}
		else {
			throw new Exception('department_error');
		}
		
		
		if( isset($data['firstname']) ) {
			$this->feed['firstname'] = htmlentities( strip_tags(trim($data['firstname'])) , ENT_QUOTES, "UTF-8");
			
			if ( mb_strlen($this->feed['firstname']) > 99 ) {
				$this->feed['firstname'] = mb_substr($this->feed['firstname'], 0, 99);
			}
		}
		else {
			$this->feed['firstname'] = NULL;
		}
		
		
		if( isset($data['lastname']) ) {
			$this->feed['lastname'] = htmlentities( strip_tags(trim($data['lastname'])) , ENT_QUOTES, "UTF-8");
			
			if ( mb_strlen($this->feed['lastname']) > 99 ) {
				$this->feed['lastname'] = mb_substr($this->feed['lastname'], 0, 99);
			}
		}
		else {
			$this->feed['lastname'] = NULL;
		}
		
		
		if( isset($data['email']) ) {
			$this->feed['email'] = htmlentities( strip_tags(trim($data['email'])) , ENT_QUOTES, "UTF-8");
			
			if ( mb_strlen($this->feed['email']) > 99 ) {
				$this->feed['email'] = mb_substr($this->feed['email'], 0, 99);
			}
		}
		else {
			$this->feed['email'] = NULL;
		}
		
		
		if( isset($data['order_num']) ) {
			$this->feed['order_num'] = (int)htmlentities( strip_tags(trim($data['order_num'])) , ENT_QUOTES, "UTF-8");
			
			if ( mb_strlen($this->feed['order_num']) > 99 ) {
				$this->feed['order_num'] = mb_substr($this->feed['order_num'], 0, 99);
			}
		}
		else {
			$this->feed['order_num'] = NULL;
		}
		
		
		if( isset($data['legalname']) ) {
			$this->feed['legalname'] = htmlentities( strip_tags(trim($data['legalname'])) , ENT_QUOTES, "UTF-8");
			
			if ( mb_strlen($this->feed['legalname']) > 99 ) {
				$this->feed['legalname'] = mb_substr($this->feed['legalname'], 0, 99);
			}
		}
		else {
			$this->feed['legalname'] = NULL;
		}
		
		
		if( isset($data['address']) ) {
			$this->feed['address'] = htmlentities( strip_tags(trim($data['address'])) , ENT_QUOTES, "UTF-8");
			
			if ( mb_strlen($this->feed['address']) > 299 ) {
				$this->feed['address'] = mb_substr($this->feed['address'], 0, 299);
			}
		}
		else {
			$this->feed['address'] = NULL;
		}
		
		
		if( isset($data['phone']) ) {
			$this->feed['phone'] = htmlentities( strip_tags(trim($data['phone'])) , ENT_QUOTES, "UTF-8");
			
			if ( mb_strlen($this->feed['phone']) > 99 ) {
				$this->feed['phone'] = mb_substr($this->feed['phone'], 0, 99);
			}
		}
		else {
			$this->feed['phone'] = NULL;
		}
		
		
		if( isset($data['phone_type']) ) {
			$this->feed['phone_type'] = htmlentities( strip_tags(trim($data['phone_type'])) , ENT_QUOTES, "UTF-8");
			
			if ( mb_strlen($this->feed['phone_type']) > 99 ) {
				$this->feed['phone_type'] = mb_substr($this->feed['phone_type'], 0, 99);
			}
		}
		else {
			$this->feed['phone_type'] = NULL;
		}
		
		
		if( isset($data['phone_time']) ) {
			$this->feed['phone_time'] = htmlentities( strip_tags(trim($data['phone_time'])) , ENT_QUOTES, "UTF-8");
			
			if ( mb_strlen($this->feed['phone_time']) > 99 ) {
				$this->feed['phone_time'] = mb_substr($this->feed['phone_time'], 0, 99);
			}
		}
		else {
			$this->feed['phone_time'] = NULL;
		}
		
		
		if( isset($data['phone2']) ) {
			$this->feed['phone2'] = htmlentities( strip_tags(trim($data['phone2'])) , ENT_QUOTES, "UTF-8");
			
			if ( mb_strlen($this->feed['phone2']) > 99 ) {
				$this->feed['phone2'] = mb_substr($this->feed['phone2'], 0, 99);
			}
		}
		else {
			$this->feed['phone2'] = NULL;
		}
		
		
		if( isset($data['phone2_type']) ) {
			$this->feed['phone2_type'] = htmlentities( strip_tags(trim($data['phone2_type'])) , ENT_QUOTES, "UTF-8");
			
			if ( mb_strlen($this->feed['phone2_type']) > 99 ) {
				$this->feed['phone2_type'] = mb_substr($this->feed['phone2_type'], 0, 99);
			}
		}
		else {
			$this->feed['phone2_type'] = NULL;
		}
		
		
		if( isset($data['phone2_time']) ) {
			$this->feed['phone2_time'] = htmlentities( strip_tags(trim($data['phone2_time'])) , ENT_QUOTES, "UTF-8");
			
			if ( mb_strlen($this->feed['phone2_time']) > 99 ) {
				$this->feed['phone2_time'] = mb_substr($this->feed['phone2_time'], 0, 99);
			}
		}
		else {
			$this->feed['phone2_time'] = NULL;
		}
		
		
		if( isset($data['business_type']) ) {
			$this->feed['business_type'] = htmlentities( strip_tags(trim($data['business_type'])) , ENT_QUOTES, "UTF-8");
			
			if ( mb_strlen($this->feed['business_type']) > 99 ) {
				$this->feed['business_type'] = mb_substr($this->feed['business_type'], 0, 99);
			}
		}
		else {
			$this->feed['business_type'] = NULL;
		}
		
		if( isset($data['theme']) ) {
			$this->feed['theme'] = htmlentities( strip_tags(trim($data['theme'])) , ENT_QUOTES, "UTF-8");
				
			if ( mb_strlen($this->feed['theme']) > 50 ) {
				$this->feed['theme'] = mb_substr($this->feed['theme'], 0, 50);
			}
		}
		else {
			$this->feed['theme'] = NULL;
		}
		
		if( isset($data['message']) ) {
			$this->feed['message'] = htmlentities( strip_tags(trim($data['message'])) , ENT_QUOTES, "UTF-8");
			
			if ( mb_strlen($this->feed['message']) > 10000 ) {
				$this->feed['message'] = mb_substr($this->feed['message'], 0, 10000);
			}
		}
		else {
			$this->feed['message'] = NULL;
		}
		
		$this->feed['hash'] = $this->generatehash();
	}
	
	public function getDbFeed($id)
	{
		$this->_name = 'feedback_1';
		$where = $this->getAdapter()->quoteInto('id = ?', (int)$id);
		$select = $this->getAdapter()	->select()
		->from($this->_name)
		->where($where);
		$stmt = $this->getAdapter()->query($select);
		// Получение данных
		$result = $stmt->fetch();
		return $result;
		
	}
	
	public function updateFeedStatus($id)
	{
		$data = array (
					'status' => 1,
				);
		$this->_name = 'feedback_1';
		$where = $this->getAdapter()->quoteInto('id = ?', (int)$id);
		
		$this->update($data, $where);
	}
	
	public function addFeed()
	{
		$this->_name = 'feedback_1';
		$this->getAdapter()->insert($this->_name, $this->feed);
		$this->feed['id'] = $this->getAdapter()->lastInsertId($this->_name);
    	
		return true;
    }
    
    public function getFeed() {
    	
    	return $this->feed;
    }
    
    /**
     * Взять строку или массив отделов
     * @param INT $department_id
     */
    public function getDepartment( $department_id = NULL )
    {	
    	$this->_name = 'departments';
    	$where = $this->getAdapter()->quoteInto('id = ?', (int)$department_id);
    	$select = $this->getAdapter()	->select()
    									->from($this->_name)
    									->where($where);
    	$stmt = $this->getAdapter()->query($select);
    	// Получение данных
    	$result = $stmt->fetch();
    	return $result;
    }
    
    
    public function getDepartments()
    {	
    	$this->_name = 'departments';
    	$select = $this->getAdapter()	->select()
    									->from($this->_name);
    	$stmt = $this->getAdapter()->query($select);
    	// Получение данных
    	$result = $stmt->fetchAll();
    	return $result;
    }
    
	protected function generatehash()
    {
		return md5( rand(10000, 100000) );
	}
}

