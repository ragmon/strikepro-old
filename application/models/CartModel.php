<?php
/**
 * CartModel
 * 
 * @author WSS http://websitespb.ru/, ffl.public@gmail.com
 * @version 
 */

require_once 'Zend/Db/Table/Abstract.php';

class Application_Model_CartModel extends Zend_Db_Table_Abstract
{
    /**
     * The default table name 
     */
    protected $_name = 'cart_basket';
    protected $_session = false;
    protected $_wss_dbg = false;

    public $_last_error = false;
    public $_current_product = false;

    public function setSession($session){
        $this->_session = $session;
    }
    //-setSession
    
    protected function _roundPrice($price){
        return round($price, 2);
    }
    
    protected function _formatPrice($price){
        return sprintf('%.2f', $price);
    }
    
    protected function _FormatOrderNum($onum){
        $fmtnum = 5;//total digits
        return sprintf('%0'.$fmtnum.'s', "$onum");
    }

    protected function _check_email($email) {
        if (preg_match("/^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z])+$/", $email)) {
            return true;
        }
        return false;
    }//-_check_email

    protected function _escape_csv_value($value) {
    	
        $value = str_replace('"', '""', $value); // First off escape all " and make them ""
        
        if(preg_match('/,/', $value) or preg_match("/\n/", $value) or preg_match('/"/', $value)) { // Check if I have any commas or new lines
            
        	return '"'.$value.'"'; // If I have new lines or commas escape them
        }
        else {
            
        	return $value; // If no new lines or commas just return the value
        }
        
    }//-_escape_csv_value

    
    //Add product to cart by SKU: total int()count as result or 0 if error
    //[$be_exact=true]: flag sets not increase quantity on update but set exact value
    public function addToCart($article_art=false, $article_count=1, $be_exact=false)
    {
        
        $this->_name = 'cart_basket';
        //$this->_wss_dbg = true;
        
        //WSS: 
        //article_art   article_cat=cat_code    article_name    article_code
        
        $cartProductCount = 0;
        
        if( !$be_exact && intval($article_count)  <  1 ) { 
            
            $article_count = 1; 
        }
        
        if( empty($article_art) ) {
            
            return $cartProductCount;
        }
        elseif(
        
            is_array(
                $this->_current_product 
                    = 
                $this->getProduct($article_art)
            ) && 
            $this->_current_product['art_insale']
        ){
            //pass, all OK: product exist && @sale
            
        }
        elseif(
            
        	is_array(
                $this->_current_product 
                    = 
                $this->getProduct($article_art)
            ) && 
            !$this->_current_product['art_insale']
        ){
            
            $cartProductCount = $article_count = 0;
            $be_exact=true;
            
        }
        else{
            
            return $cartProductCount;
        }
        
        $sessid = Zend_Session::getId();

        
        
        if($this->_wss_dbg) {
            
            echo "<br />\n".'sessid:'.$sessid."<br />\n";
        }
        
        
        
        $where = $this->getAdapter()->quoteInto('cb.sessid = ?', "$sessid");
        $andwhere = $this->getAdapter()->quoteInto('cb.product_art = ?', "$article_art");
        
        $select = $this->getAdapter()   ->select()
                                        ->from(
                                            array('cb' => 'cart_basket'),
                                            array('cb.sessid', 'cb.product_art', 'cb.product_count')
                                        )
                                        ->where($where)->where($andwhere);
        
        if( $this->_wss_dbg ) {
            
            echo "<br />\n";
            $sql = $select->__toString();
            echo "$sql\n"."<br />\n";
        }
        
        //$statement = $this->getAdapter()->query($select);
        //$result = $statement->fetchAll();
        //$result = $this->getAdapter()->fetchRow($select)
        
        if( $result = $this->getAdapter()->fetchRow($select) ) {
            
            //update
            if( $be_exact ) {
                
                $cartProductCount = $article_count;
            }
            else {
                
                $cartProductCount = $result['product_count'] + $article_count;
            }
            
            
            if( $this->_wss_dbg ) {
                
                echo "<br />\n";
                echo "UPDATING<br />\n";
                print_r($result);
                echo "<br />\n";
                echo "count:".($result['product_count'])."<br />\n";
                echo "newcount:".($cartProductCount)."<br />\n";
                echo "<br />\n";
            }
            
            //99 limit
            if( $cartProductCount > 99 ) {
                
                $cartProductCount = 99;
            }
            
            $data = Array(
                'product_count' => $cartProductCount,
            );

            $where = $this->getAdapter()->quoteInto('sessid = ?', "$sessid");
            $andwhere = $this->getAdapter()->quoteInto('product_art = ?', "$article_art");
            $this->update($data, Array($where, $andwhere));
            
        }
        else{
            //insert, $be_exact=true as is )
            $cartProductCount = $article_count;
            
            //$table = new Application_Model_CartModel();
            if($this->_wss_dbg){
                echo "<br />\n";
                echo "INSERTING<br />\n";
            }
            
            //99 limit
            if( $cartProductCount > 99 ) {
                
                $cartProductCount = 99;
            }
            
            $data = Array(
                'sessid' => "$sessid",
                'product_art' => "$article_art",
                'product_count' => $cartProductCount,
                'ins' => new Zend_Db_Expr('NOW()'),
            );
            $this->insert($data);
        }
        
        return $cartProductCount;
    }
    //-addToCart
    
    
    //Full cart dump: array
    public function getCartProducts()
    {
        
        $this->_name = 'cart_basket';
        
        $sessid = Zend_Session::getId();        
      
        
        $where = $this->getAdapter()->quoteInto('cb.sessid = ?', "$sessid");
        
        $select = $this->getAdapter()   ->select()
                                        ->from(
                                            array('cb' => 'cart_basket'),
                                            array('cb.sessid', 'cb.product_art', 'cb.product_count')
                                        )
                                        ->joinLeft(
                                            array('ca' => 'catalog_articles'),
                                            'ca.article_art = cb.product_art', 
                                            array(
                                                'ca.article_cat', 'ca.article_name', 
                                                'ca.article_code', 'ca.article_image', 
                                                'ca.art_insale', 'ca.art_price','ca.article_code' 
                                            )
                                        )
                                        ->joinLeft(
                                            array('cc' => 'catalog_cats'),
                                            'cc.cat_code = ca.article_cat', 
                                            array(
                                                'cc.cat_name', 'cc.cat_codename', 'cc.cat_code', 
                                                'cc.cat_link',
                                            )
                                        )
                                        ->where($where)
                                        ->order('cb.ins ASC');

        
        $statement = $this->getAdapter()->query($select);
        $result = $statement->fetchAll();
        
        
        foreach(array_keys($result) as $key) {
            
            $result[$key]['fullname_fmt'] =  trim($result[$key]['article_code'] . ' ' . $result[$key]['article_name']);            
            $result[$key]['art_price_fmt']	= $this->_formatPrice($result[$key]['art_price']);
            $result[$key]['row_total']		= $this->_roundPrice($result[$key]['product_count']*$this->_roundPrice($result[$key]['art_price'], 2), 2);
            $result[$key]['row_total_fmt']	= $this->_formatPrice($result[$key]['row_total']);
        }
        
        return $result;
    }
    //-getCartProducts
    
    
    //Product specs by SKU: array or false
    public function getProduct( $article_art=false )
    {
        $this->_name = 'cart_basket';
        
        if( empty( $article_art ) ){
            
            return false;
        }
        
        $where = $this->getAdapter()->quoteInto('ca.article_art = ?', "$article_art");
        
        $select = $this->getAdapter()   ->select()
                                        ->from(
                                            array('ca' => 'catalog_articles'),
                                            array(
                                                'ca.article_cat', 'ca.article_name', 
                                                'ca.article_code', 'ca.article_image', 
                                                'ca.art_insale', 'ca.art_price', 
                                            )
                                        )
                                        ->joinLeft(
                                            array('cc' => 'catalog_cats'),
                                            'cc.cat_code = ca.article_cat', 
                                            array(
                                                'cc.cat_name', 'cc.cat_codename', 'cc.cat_code', 
                                                'cc.cat_link',
                                            )
                                        )
                                        ->where($where);
        
        if( $result = $this->getAdapter()->fetchRow($select) ) {
            
            return $result;
        }
        
        return false;
    }
    //-getProduct
        
    
    //Set product quantity: if !exist - add, if exist - update, if qty==0 - delete
    public function setQuantityExact($arts_qtys = Array(), $allow_delete=true){

        $this->_name = 'cart_basket';
        //$this->_wss_dbg = true;
        
        $sessid = Zend_Session::getId();
        
        if(!is_array($arts_qtys)){
            return false;
        }

        $where = $this->getAdapter()->quoteInto('sessid = ?', "$sessid");
        
        foreach (array_keys($arts_qtys) as $art){
            $qty = intval($arts_qtys[$art]);
            $andwhere = $this->getAdapter()->quoteInto('product_art = ?', "$art");
            if($qty==0 && $allow_delete){
                $this->delete(array($where, $andwhere));
            }
            else{
                //$be_exact=true: 
                //flag sets not increase quantity on update but set exact value
                $this->addToCart($article_art=$art, $article_count=$qty, $be_exact=true);
            }
        }
        
        return true;
    }
    //-setQuantityExact
        
    
    //Emptyes cart records by current session
    public function emptyCart(){

        $this->_name = 'cart_basket';
        
        $sessid = Zend_Session::getId();

        $where = $this->getAdapter()->quoteInto('sessid = ?', "$sessid");
        $this->delete($where);
        
        return true;
    }
    //-emptyCart
        
    
    //Emptyes cart records by current session
    public function StoreCartToOrder($ordercontacts, $dlvr_mode_idx=false, $extraAddrFields=array()){

        $this->_name = 'cart_basket';
        //$this->_wss_dbg = true;
        
        $sessid = Zend_Session::getId();
        
        $cart = $this->getCartProducts();
        
        if(!count($cart)){return false;}
        
        $total = 0;
        
        foreach(  array_keys( $cart )  AS  $key  ) {
            
            $cart[$key]['row_total'] = round(  $cart[$key]['product_count']  *  round( $cart[$key]['art_price'], 2 ), 2  );
            $total += $cart[$key]['row_total'];
        }
        
        
        $delivery_cfg = new Zend_Config_Ini('../application/configs/strikepro.ini', 'delivery');
        $delivery_product = false;
        
        
        $dlv_idx = $deliverycosts = 0;
        $deliveryproduct = '';
        
        if(  !intval( $dlvr_mode_idx )   &&   array_key_exists( 'dlvr_mode_idx', $_REQUEST )  ) {
            
            $dlvr_mode_idx = intval($_REQUEST['dlvr_mode_idx']);
        }
        
        if(  !intval( $dlvr_mode_idx )  ) {
            
            $dlvr_mode_idx = 1;
        }
        
        if(  intval( $dlvr_mode_idx )  >  count( $delivery_cfg->deliverynames )  ) {
            
            $dlvr_mode_idx = 1;
        
        }
        
        foreach( $delivery_cfg->deliverynames as $dlv_name ) {
            
            $dlv_idx++;
            
            if( $dlvr_mode_idx == $dlv_idx ) {
                
                $deliveryproduct	= $dlv_name;
                $deliverycosts 		= $delivery_cfg->deliveryprices->$dlv_idx;
            }
        }
        
        $deliverycosts 		= floatval($deliverycosts);
        $deliveryproduct 	= ' ' . $deliveryproduct;
        
        if( $delivery_cfg->freedeliveryamount ) {
            
            
            if( $total  >  $delivery_cfg->freedeliveryamount ) {
                
                //free
                $delivery_product = true;
                $deliverycosts = 0;
            }
            else {
                
                $total += $deliverycosts;
                $delivery_product = true;
            }
            
            
            
        }
        else {
            
            
            $total += $deliverycosts;
            $delivery_product = true;
            
            
        }
        
        $this->_name = 'cart_orders';
        
        $ordercontacts['ins'] = new Zend_Db_Expr('NOW()');
        $ordercontacts['total'] = $total;
        $ordercontacts['status'] = 'accepting';
        
        
        if(!array_key_exists('dbg', $ordercontacts) || !$ordercontacts['dbg'] || empty($ordercontacts['dbg'])){
            
            $ordercontacts['dbg'] = false;
        }
        

        if(preg_match('/^\d+$/', $ordercontacts['city'])) {
        
                $this->_name = 'jos_kladr';
                
                $where = $this->getAdapter()->quoteInto(' `k`.`code`=? ', $ordercontacts['city']);
                         
                $select = $this->getAdapter()   ->select()
                                                ->from(
                                                    array('k' => 'jos_kladr'),
                                                    array(
                                                        'k.code','k.index','k.name','k.socr',
                                                    )
                                                )
                                                ->where($where)
                                                ->order('k.name ASC');
            
                $result = $this->getAdapter()->fetchRow($select);
            
            if($result) {
            
                $ordercontacts['city'] = $result['name'] . ' ' . $result['socr'] . '.';       
            }
            
        }
        
        $this->_name = 'cart_orders';
        $order_id = $this->insert($ordercontacts);
        
        $this->_name = 'cart_order_products';
         
        $arts = array();
        

        foreach(  array_keys( $cart ) as $key  ) {
            
            if(  intval( $cart[$key]['product_count'] )  ==  0  ) {
                
                // no qty = 0 products @order
                continue;
            }
            
           array_push(  $arts  ,  $cart[$key]['product_art'] . '*' . $cart[$key]['product_count']  );
            
            $productrow = array(
                
            	'order_id' 		=> $order_id,
                'product_art' 	=> $cart[$key]['product_art'],
                'product_price' => round($cart[$key]['art_price'], 2),
                'product_count' => $cart[$key]['product_count'],
                'product_total' => $cart[$key]['row_total'],
                'product_name' 	=> $cart[$key]['fullname_fmt'],
                'ins' 			=> new Zend_Db_Expr('NOW()'),
            );
            
           
            
            $this->insert($productrow);
            
            //AlexTaran edit 23-11-12
            unset($productrow['ins']);
            $fullorder['goodies'][] = $productrow;
        }
        
        if( $delivery_product ) {
            
			$delivery_product = array(
	                
					'order_id' => $order_id,
	                'product_art' => 'DLVR-00'.($dlvr_mode_idx),
	                'product_price' => $deliverycosts,
	                'product_count' => 1,
	                'product_total' => $deliverycosts,
	                'product_name' =>  $deliveryproduct,
	                'ins' => new Zend_Db_Expr('NOW()'),
				);
            
            $this->insert($delivery_product);
            
            //AlexTaran edit 23-11-12
            unset($delivery_product['ins']);
            $fullorder['delivery'] = $delivery_product;
        }


        $arts = implode(',', $arts);

        $this->emptyCart();
		
        //AlexTaran edit 23-11-12
        $fullorder['total'] 	= $total;
        $fullorder['order_id'] 	= $order_id;
        $fullorder['contact']	= $ordercontacts;
        
        unset ($fullorder['contact']['dbg']);
        unset ($fullorder['contact']['ins']);
        unset ($fullorder['contact']['total']);
        unset ($fullorder['contact']['status']);
        
        
        return array(
        	
        	'fullorder' 	=> $fullorder, // AlexTaran 23-11-12
            'order_id'		=> $order_id,
            'total'			=> $total,
            'product_arts' 	=> $arts,
        );
    }
    //-StoreCartToOrder
    
    
    //Return order data or false if order_id !exist
    public function getOrder($order_id){
        
        $this->_name = 'cart_orders';
        
        $where = $this->getAdapter()->quoteInto('order_id = ?', $order_id);
        
        $select = $this->getAdapter()   ->select()
                                        ->from(
                                            array('co' => 'cart_orders'),
                                            array(
                                                'co.order_id', 
                                                'co.fio', 'co.email', 
                                                'co.phone', 'co.city', 
                                                'co.address', 'co.comments', 
                                                'co.total', 'co.status', 
                                                'ins_fmt' => new Zend_Db_Expr('DATE_FORMAT(co.ins, \'%d.%m.%Y %H:%i\')') 
                                            )
                                        )
                                        ->where($where);
        if($order_contacts = $this->getAdapter()->fetchRow($select)){
            //order exists
            
        }
        else{
            return false;
        }
        
        $this->_name = 'cart_order_products';
        $select = $this->getAdapter()   ->select()
                                        ->from(
                                            array('cop' => 'cart_order_products'),
                                            array(
                                                'cop.order_id', 
                                                'cop.product_art', 
                                                'cop.product_price', 
                                                'cop.product_count', 
                                                'cop.product_total', 
                                                'cop.product_name'
                                            )
                                        )
                                        ->joinLeft(
                                            array('ca' => 'catalog_articles'),
                                            'ca.article_art = cop.product_art', 
                                            array(
                                                'ca.article_cat', 'ca.article_name', 
                                                'ca.article_code', 'ca.article_image', 
                                                'ca.art_insale', 'ca.art_price',
                                            )
                                        )
                                        ->joinLeft(
                                            array('cc' => 'catalog_cats'),
                                            'cc.cat_code = ca.article_cat', 
                                            array(
                                                'cc.cat_name', 'cc.cat_codename', 'cc.cat_code', 
                                                'cc.cat_link', //'cc.cat_price', 'cc.cat_insale', 
                                            )
                                        )
                                        ->where($where);

        $statement = $this->getAdapter()->query($select);
        $order_products = $statement->fetchAll();

        $this->_name = 'cart_payments';
        //$this->_wss_dbg = true;
        
        $where = $this->getAdapter()->quoteInto('orderId = ?', $order_id);
        
        $select = $this->getAdapter()   ->select()
                                        ->from(
                                            array('cp' => 'cart_payments'),
                                            array(
                                                'cp.eshopId', 'cp.paymentId', 
                                                'cp.orderId', 'cp.eshopAccount', 
                                                'cp.serviceName', 'cp.recipientAmount', 
                                                'cp.recipientCurrency', 'cp.paymentStatus', 
                                                'cp.userName', 'cp.userEmail', 
                                                'cp.paymentData', 'cp.secretKey', 
                                                'cp.hash', 'cp.userFieldS', 
                                                'ins_fmt' => new Zend_Db_Expr('DATE_FORMAT(cp.ins, \'%d.%m.%Y %H:%i\')') 
                                            )
                                        )
                                        ->where($where);
        
        $statement = $this->getAdapter()->query($select);
        $order_payments = $statement->fetchAll();
        
        return array(
            'order_contacts' => $order_contacts,
            'order_products' => $order_products,
            'order_payments' => $order_payments,
        );
    }//-getOrder
    
    public function getavOrder($order_id){
    
    	$this->_name = 'cart_orders';
    
    	$where = $this->getAdapter()->quoteInto('order_id = ?', $order_id);
    
    	$select = $this->getAdapter()   ->select()
    	->from(
    			array('co' => 'cart_orders'),
    			array(
    					'co.order_id',
    					'co.fio', 'co.email',
    					'co.phone', 'co.city',
    					'co.address', 'co.comments',
    					'co.total', 'co.status',
    					'ins_fmt' => new Zend_Db_Expr('DATE_FORMAT(co.ins, \'%d.%m.%Y %H:%i\')')
    			)
    	)
    	->where($where);
    
    	if($order_contacts = $this->getAdapter()->fetchRow($select)){
    		//order exists
    	}
    	else{
    		return false;
    	}
    
    	$this->_name = 'cart_order_products';
    	$select = $this->getAdapter()->select()
    	->from(
    			array('cop' => 'cart_order_products'),
    			array(
    					'cop.order_id',
    					'cop.product_art',
    					'cop.product_price',
    					'cop.product_count',
    					'cop.product_total',
    					'cop.product_name'
    			)
    	)
    	->joinLeft(
    			array('ca' => 'catalog_articles'),
    			'ca.article_art = cop.product_art',
    			array(
    					'ca.article_cat', 'ca.article_name',
    					'ca.article_code', 'ca.article_image',
    					'ca.art_insale', 'ca.art_price',
    			)
    	)
    	->joinLeft(
    			array('cc' => 'catalog_cats'),
    			'cc.cat_code = ca.article_cat',
    			array(
    					'cc.cat_name', 'cc.cat_codename', 'cc.cat_code',
    					'cc.cat_link', //'cc.cat_price', 'cc.cat_insale',
    			)
    	)
    	->where($where)
    	->order('ca.article_name ASC');
    
    	$statement = $this->getAdapter()->query($select);
    	$order_products = $statement->fetchAll();
    
    	$this->_name = 'cart_payments';
    	//$this->_wss_dbg = true;
    
    	$where = $this->getAdapter()->quoteInto('orderId = ?', $order_id);
    
    	$select = $this->getAdapter()   ->select()
    	->from(
    			array('cp' => 'cart_payments'),
    			array(
    					'cp.eshopId', 'cp.paymentId',
    					'cp.orderId', 'cp.eshopAccount',
    					'cp.serviceName', 'cp.recipientAmount',
    					'cp.recipientCurrency', 'cp.paymentStatus',
    					'cp.userName', 'cp.userEmail',
    					'cp.paymentData', 'cp.secretKey',
    					'cp.hash', 'cp.userFieldS',
    					'ins_fmt' => new Zend_Db_Expr('DATE_FORMAT(cp.ins, \'%d.%m.%Y %H:%i\')')
    			)
    	)
    	->where($where);
    
    	$statement = $this->getAdapter()->query($select);
    	$order_payments = $statement->fetchAll();
    
    	return array(
    			'order_contacts' => $order_contacts,
    			'order_products' => $order_products,
    			'order_payments' => $order_payments,
    	);
    }//-getOrder
    
    
    //stores payment @table
    public function paymentStore($payment_data){
        
        $this->_name = 'cart_payments';
        //$this->_wss_dbg = true;
        
        if(!$payment_data['paymentId'] || !$payment_data['orderId']){
            //smth wrong
            return false;
        }
        
        $this->insert($payment_data);
        
        $mail = new Zend_Mail('UTF-8');
        $mail->setBodyText(
            'На сайте учтен платеж по заказу #'.
            ($this->_FormatOrderNum(($payment_data['orderId'])))."\n".
            'Сумма: '.($payment_data['recipientAmount']).($payment_data['recipientCurrency'])."\n"
        );
        
        $mailconfig = new Zend_Config_Ini('../application/configs/strikepro.ini', 'emailnotifications');
        
        $mail->setFrom($mailconfig->fromemail, $mailconfig->fromname);
        
        foreach($mailconfig->rcpts as $rcpt) {
            $rcpt = explode('|', $rcpt);
            if(!array_key_exists(1, $rcpt) || empty($rcpt[1])){$rcpt[1]='Manager';}
            $mail->addTo($rcpt[0], $rcpt[1]);
        }
        
        if( !empty($payment_data['userEmail']) && $this->_check_email($payment_data['userEmail']) ) {
            
            $mail->addTo($payment_data['userEmail'], 'Client');
        }
        
        $mail->setSubject('StrikePro: платеж по заказу #'.$payment_data['orderId']);
        $mail->send();
        
        return true;
    }//-paymentStore
    

    //Product specs by SKU: array or false
    public function getKladrResults($idx=false, $cty=false, $str=false, $dom=false, $subaction=false)
    {
        $this->_name = 'jos_kladr';
        //$this->_name = 'jos_kladr_street';
        //$this->_wss_dbg = true;
        
        $idxobj = array();
        $ctyobj = array();
        $strobj = array();
        $domobj = array();
        
        if($subaction){
            
            /*
             * ChildType
             * -1 регионы
             * 5 - районы
             * 6 - насел. пункты
             * 8 - улицы
             * 
             * ParentId
             * регион, насел. пункт, район???
             * 
                СС РРР ГГГ ППП АА, где
                СС – код субъекта Российской Федерации (региона), коды регионов представлены в Приложении 2 к Описанию классификатора адресов Российской Федерации (КЛАДР);
                РРР – код района;
                ГГГ – код города;      
                ППП – код населенного пункта,
                АА – признак актуальности адресного объекта.
             * 
            */
            
            
            if($subaction && $subaction=='getIdxByStrHouseNum'){
                
                $stridx = $_REQUEST['strcode'];
                $housenum = $_REQUEST['housenum'];
                
                $this->_name = 'jos_kladr_street';
                
                $where = $this->getAdapter()->quoteInto(' `ks`.`code` LIKE ? ', $stridx);
                
                $select = $this->getAdapter()   ->select()
                                                ->from(
                                                    array('ks' => 'jos_kladr_street'),
                                                    array(
                                                        'ks.name', 'ks.code', 'ks.index',
                                                    )
                                                )
                                                ->where($where)
                                                //->limit(0, 10000)
                                                ->order('ks.name ASC');
                
            
                if($this->_wss_dbg){
                    echo "<br />\n";
                    $sql = $select->__toString();
                    echo "$sql\n"."<br />\n";
                }
                
                $statement = $this->getAdapter()->query($select);
                $result = $statement->fetchAll();
                
                $idx = '-1';
                if(count($result)==1){
                    $idx = $result[0]['index'];
                    
                    if($idx){
                        //pass
                    }
                    else{
                        
                        
                        
                        $this->_name = 'jos_kladr_doma';
                        
                        $strrow = $result[0];
                        $avaliableidxes = Array();
                        
                        $where = $this->getAdapter()->quoteInto(' `kd`.`code` LIKE ? ', $strrow['code'].'%');
                        
                        $select = $this->getAdapter()   ->select()
                                                        ->from(
                                                            array('kd' => 'jos_kladr_doma'),
                                                            array(
                                                                'kd.name', 'kd.code', 'kd.index', 'kd.socr'
                                                            )
                                                        )
                                                        ->where($where);
                        
                        if($this->_wss_dbg){
                            echo "<br />\n";
                            $sql = $select->__toString();
                            echo "$sql\n"."<br />\n";
                        }
                        
                        $statement = $this->getAdapter()->query($select);
                        $result = $statement->fetchAll();
                    
                            foreach ($result as $domrow){
                                $domprenums = $domrow['name'];
                                $domprenums = explode(',', $domprenums);
                                foreach ($domprenums as $domnum){
                                    $domnum = trim($domnum);
                                    $dommatches=array();
                                    if(preg_match('/([^\(]*)?[\(]?(\d+\D*)\-(\d+\D*)/', $domnum, $dommatches)){
                                        $domtype = $dommatches[1];
                                        $dompre = intval($dommatches[2]);
                                        $domafter = intval($dommatches[3]);
                                        
                                        if(intval($housenum) >= $dompre && intval($housenum) <= $domafter){
                                            if(
                                                (!$domtype || empty($domtype))
                                                    ||
                                                (
                                                    $domtype && $domtype == 'Н' && (intval($housenum)%2)
                                                )
                                                    ||
                                                (
                                                    $domtype && $domtype == 'Ч' && !(intval($housenum)%2)
                                                )
                                            ){

                                                $domrow['name'] = str_replace('Ч(', 'Четн.(', $domrow['name']);
                                                $domrow['name'] = str_replace('Н(', 'НеЧетн.(', $domrow['name']);
                                                array_push($avaliableidxes, $domrow);
                                            }
                                        }
                                    }
                                    elseif(preg_match('/(\d+)/', $domnum, $dommatches)){
                                        //$domnum = intval($domnum);
                                        $domnum = intval($dommatches[1]);
                                        if($domnum==intval($housenum)){
                                            array_push($avaliableidxes, $domrow);
                                        }
                                    }
                                    else{
                                        //pass
                                        
                                    }
                                    
                                }
                            }
                        
                        if(count($avaliableidxes)==1){
                            $idx = $avaliableidxes[0]['index'];
                        }
                        else{
                            //pass
                        }
                        
                    }
                    
                }
                else{
                    //pass
                }
                
                //echo $_REQUEST['strcode'].'ggg';
                //$this->_wss_dbg =  true;
                if(empty($idx) || $idx == '-1'){
                    //no index, search parent situation
                    
                    //local city
                        $subcode = substr($stridx, 0, 11);
                        //$subcode .= (str_repeat('_', 3));
                        $subcode .= '00';
                        $this->_name = 'jos_kladr';
                        
                        $where = $this->getAdapter()->quoteInto(' `k`.`code`=? ', $subcode);
                                 
                        $select = $this->getAdapter()   ->select()
                                                        ->from(
                                                            array('k' => 'jos_kladr'),
                                                            array(
                                                                'k.code','k.index'
                                                            )
                                                        )
                                                        ->where($where)
                                                        ->order('k.name ASC');

                        if($this->_wss_dbg){
                            echo "<br />\n";
                            $sql = $select->__toString();
                            echo "$sql\n"."<br />\n";
                        }
                        
                        $statement = $this->getAdapter()->query($select);
                        $result = $statement->fetchAll();
                        
                        if(count($result)==1 && array_key_exists('index', $result[0])){
                            $idx = $result[0]['index'];
                        }
                    
                    
                    //city
                    if(empty($idx) || $idx == '-1'){
                        
                        $subcode = substr($stridx, 0, 8);
                        //$subcode .= (str_repeat('_', 3));
                        $subcode .= (str_repeat('0', 3));
                        $subcode .= '00';
                        $this->_name = 'jos_kladr';
                        
                        $where = $this->getAdapter()->quoteInto(' `k`.`code`=? ', $subcode);
                                 
                        $select = $this->getAdapter()   ->select()
                                                        ->from(
                                                            array('k' => 'jos_kladr'),
                                                            array(
                                                                'k.code','k.index'
                                                            )
                                                        )
                                                        ->where($where)
                                                        ->order('k.name ASC');

                        if($this->_wss_dbg){
                            echo "<br />\n";
                            $sql = $select->__toString();
                            echo "$sql\n"."<br />\n";
                        }
                        
                        $statement = $this->getAdapter()->query($select);
                        $result = $statement->fetchAll();
                        
                        if(count($result)==1 && array_key_exists('index', $result[0])){
                            $idx = $result[0]['index'];
                        }

                    }
                    
                    //area
                    if(empty($idx) || $idx == '-1'){
                        
                        $subcode = substr($stridx, 0, 5);
                        //$subcode .= (str_repeat('_', 3));
                        $subcode .= (str_repeat('0', 3));
                        $subcode .= (str_repeat('0', 3));
                        $subcode .= '00';
                        $this->_name = 'jos_kladr';
                        
                        $where = $this->getAdapter()->quoteInto(' `k`.`code`=? ', $subcode);
                                 
                        $select = $this->getAdapter()   ->select()
                                                        ->from(
                                                            array('k' => 'jos_kladr'),
                                                            array(
                                                                'k.code','k.index'
                                                            )
                                                        )
                                                        ->where($where)
                                                        ->order('k.name ASC');

                        if($this->_wss_dbg){
                            echo "<br />\n";
                            $sql = $select->__toString();
                            echo "$sql\n"."<br />\n";
                        }
                        
                        $statement = $this->getAdapter()->query($select);
                        $result = $statement->fetchAll();
                        
                        if(count($result)==1 && array_key_exists('index', $result[0])){
                            $idx = $result[0]['index'];
                        }

                    }
                    
                    //region
                    if(empty($idx) || $idx == '-1'){
                        
                        $subcode = substr($stridx, 0, 2);
                        //$subcode .= (str_repeat('_', 3));
                        $subcode .= (str_repeat('0', 3));
                        $subcode .= (str_repeat('0', 3));
                        $subcode .= (str_repeat('0', 3));
                        $subcode .= '00';
                        $this->_name = 'jos_kladr';
                        
                        $where = $this->getAdapter()->quoteInto(' `k`.`code`=? ', $subcode);
                                 
                        $select = $this->getAdapter()   ->select()
                                                        ->from(
                                                            array('k' => 'jos_kladr'),
                                                            array(
                                                                'k.code','k.index'
                                                            )
                                                        )
                                                        ->where($where)
                                                        ->order('k.name ASC');

                        if($this->_wss_dbg){
                            echo "<br />\n";
                            $sql = $select->__toString();
                            echo "$sql\n"."<br />\n";
                        }
                        
                        $statement = $this->getAdapter()->query($select);
                        $result = $statement->fetchAll();
                        
                        if(count($result)==1 && array_key_exists('index', $result[0])){
                            $idx = $result[0]['index'];
                        }

                    }
                    
                    if(empty($idx) || $idx == '-1'){$idx = '-1';}
                    
                }
                
                return array('houseindex' => $idx);
                
            }
            elseif(array_key_exists('ChildType', $_REQUEST) && intval($_REQUEST['ChildType'])==8){
                //$this->_wss_dbg = true;
                /*
                $prearea = $_REQUEST['ParentId'];
                $prearea = substr($prearea, 0 , 11);
                $area = substr($prearea, 0 , 2);
                */
                
                $area = $_REQUEST['ParentId'];
                $area = substr($area, 0 , 11);

                /*
                if(substr($area, 2 , 3)=='000' && substr($area, 5 , 3)=='000'){
                    
                    $prearea = substr($area, 0 , 2);
                    
                    //$prearea .= '___00000000';

                    $this->_name = 'jos_kladr';
                    
                    $where = $this->getAdapter()->quoteInto(' `k`.`code` LIKE ? ', ($prearea.'___00000000'));
                    $andwhere = $this->getAdapter()->quoteInto(' `k`.`code`!=? ', ($prearea.'00000000000'));
                             
                    $select = $this->getAdapter()   ->select()
                                                    ->from(
                                                        array('k' => 'jos_kladr'),
                                                        array(
                                                            'k.code'
                                                        )
                                                    )
                                                    ->where($where)
                                                    ->where($andwhere)
                                                    ->order('k.name ASC');

                    if($this->_wss_dbg){
                        echo "<br />\n";
                        $sql = $select->__toString();
                        echo "$sql\n"."<br />\n";
                    }
                    
                    $statement = $this->getAdapter()->query($select);
                    $result = $statement->fetchAll();
                    
                    if(count($result) && array_key_exists('code', $result[0])){
                        $area = $result[0]['code'];
                    }
                    
                }
                
                //if(substr($area, 2 , 3)!='000' && substr($area, 5 , 3)=='000'){
                if(substr($area, 5 , 3)=='000'){
                    
                    $prearea = substr($area, 0 , 5);
                    
                    //$prearea .= '___00000000';

                    $this->_name = 'jos_kladr';
                    
                    $where = $this->getAdapter()->quoteInto(' `k`.`code` LIKE ? ', ($prearea.str_repeat('_', 6).'00'));
                    $andwhere = $this->getAdapter()->quoteInto(' `k`.`code`!=? ', ($prearea.'00000000'));
                             
                    $select = $this->getAdapter()   ->select()
                                                    ->from(
                                                        array('k' => 'jos_kladr'),
                                                        array(
                                                            'k.code'
                                                        )
                                                    )
                                                    ->where($where)
                                                    ->where($andwhere)
                                                    ->order('k.name ASC');

                    if($this->_wss_dbg){
                        echo "<br />\n";
                        $sql = $select->__toString();
                        echo "$sql\n"."<br />\n";
                    }
                    
                    $statement = $this->getAdapter()->query($select);
                    $result = $statement->fetchAll();
                    
                    if(count($result) && array_key_exists('code', $result[0])){
                        $area = $result[0]['code'];
                    }
                    
                }
                
                $prearea = substr($area, 0 , 11);
                $area = substr($prearea, 0 , 2);
                
                //СС2 РРР5 ГГГ8 ППП11 УУУУ АА,
                if(substr($prearea,  2, 9)=='000000000'){
                    $area = substr($prearea, 0 , 2) . (str_repeat('_', 9));
                }
                else{
                    if(substr($prearea,  2, 6)!='000000' && substr($prearea,  8, 3)=='000'){
                        $area = substr($prearea, 0 , 8) . (str_repeat('_', 3));
                    }
                    else{
                        if(substr($prearea,  2, 3)!='000' && substr($prearea,  5, 3)=='000'){
                            $area = substr($prearea, 0 , 5) . (str_repeat('_', 6));
                        }
                        else{
                            $area = $prearea;
                        }
                    }
                }
                */
                
                
                $area .= str_repeat('_', 4);//street
                //$area .= str_repeat('_', 2);//state
                $area .= '00';//state
                
                $where = $this->getAdapter()->quoteInto(' `k`.`code` LIKE ? ', $area);
                
                $this->_name = 'jos_kladr_street';
                
                $where = $this->getAdapter()->quoteInto(' `ks`.`code` LIKE ? ', $area);
                
                $select = $this->getAdapter()   ->select()
                                                ->from(
                                                    array('ks' => 'jos_kladr_street'),
                                                    array(
                                                        'ks.name', 'ks.code', 'ks.index', 'ks.socr',
                                                        'Text' => new Zend_Db_Expr('CONCAT_WS(\' \', `ks`.`name`, CONCAT_WS(\'\', `ks`.`socr`, \'.\'))'), 
                                                        'Value' => 'ks.code',
                                                    )
                                                )
                                                ->where($where)
                                                //->limit(0, 10000)
                                                ->order('ks.name ASC');
                
            
                if($this->_wss_dbg){
                    echo "<br />\n";
                    $sql = $select->__toString();
                    echo "$sql\n"."<br />\n";
                }
                
                $statement = $this->getAdapter()->query($select);
                $result = $statement->fetchAll();
                
                //array_unshift($result, Array('Text' => '', 'Value' => '-1'));
                array_unshift($result, Array('Text' => '', 'Value' => $_REQUEST['ParentId']));
                
                return $result;
                
            }
            elseif(array_key_exists('ChildType', $_REQUEST) && intval($_REQUEST['ChildType'])==6){
                
                //city
                
                $area = $_REQUEST['ParentId'];
                
                if(0 && substr($area, 2 , 3)=='000'){//Ulianovsk fix
                    
                    $prearea = substr($area, 0 , 2);
                    
                    //$prearea .= '___00000000';

                    $this->_name = 'jos_kladr';
                    
                    $where = $this->getAdapter()->quoteInto(' `k`.`code` LIKE ? ', ($prearea.'___00000000'));
                    $andwhere = $this->getAdapter()->quoteInto(' `k`.`code`!=? ', ($prearea.'00000000000'));
                             
                    $select = $this->getAdapter()   ->select()
                                                    ->from(
                                                        array('k' => 'jos_kladr'),
                                                        array(
                                                            'k.code'
                                                        )
                                                    )
                                                    ->where($where)
                                                    ->where($andwhere)
                                                    ->order('k.name ASC');

                    if($this->_wss_dbg){
                        echo "<br />\n";
                        $sql = $select->__toString();
                        echo "$sql\n"."<br />\n";
                    }
                    
                    $statement = $this->getAdapter()->query($select);
                    $result = $statement->fetchAll();
                    
                    if(count($result) && array_key_exists('code', $result[0])){
                        $area = $result[0]['code'];
                    }
                    
                }
                if(substr($area, 2 , 3)=='000'){//Ulianovsk fix
                    $area = substr($area, 0 , 2) . str_repeat('_', 3);
                }
                
                $area = substr($area, 0 , 5);
                $area .= str_repeat('_', 6).'00';

                $this->_name = 'jos_kladr';
                
                $where = $this->getAdapter()->quoteInto(' `k`.`code` LIKE ? ', $area);
                $andwhere = $this->getAdapter()->quoteInto(' `k`.`code`!=? ', $_REQUEST['ParentId']);
                         
                $select = $this->getAdapter()   ->select()
                                                ->from(
                                                    array('k' => 'jos_kladr'),
                                                    array(
                                                        'k.name', 'k.code', 'k.index', 'k.socr',
                                                        'Text' => new Zend_Db_Expr('CONCAT_WS(\' \', `k`.`index`, `k`.`name`, CONCAT_WS(\'\', `k`.`socr`, \'.\'))'), 
                                                        'Value' => 'k.code',
                                                        
                                                    )
                                                )
                                                ->where($where)
                                                ->where($andwhere)
                                                ->order('k.name ASC');
            
                if($this->_wss_dbg){
                    echo "<br />\n";
                    $sql = $select->__toString();
                    echo "$sql\n"."<br />\n";
                }
                
                $statement = $this->getAdapter()->query($select);
                $result = $statement->fetchAll();
                
                //array_unshift($result, Array('Text' => '', 'Value' => '-1'));
                array_unshift($result, Array('Text' => '', 'Value' => $_REQUEST['ParentId']));
                
                return $result;
                
            }
            elseif(array_key_exists('ChildType', $_REQUEST) && intval($_REQUEST['ChildType'])==5){
                
                //area
                
                $area = $_REQUEST['ParentId'];
                $area = substr($area, 0 , 2);
                $area .= '___00000000';

                $this->_name = 'jos_kladr';
                
                $where = $this->getAdapter()->quoteInto(' `k`.`code` LIKE ? ', $area);
                $andwhere = $this->getAdapter()->quoteInto(' `k`.`code`!=? ', $_REQUEST['ParentId']);
                         
                $select = $this->getAdapter()   ->select()
                                                ->from(
                                                    array('k' => 'jos_kladr'),
                                                    array(
                                                        'k.name', 'k.code', 'k.index', 'k.socr',
                                                        'Text' => new Zend_Db_Expr('CONCAT_WS(\' \', `k`.`name`, CONCAT_WS(\'\', `k`.`socr`, \'.\'))'), 
                                                        'Value' => 'k.code',
                                                        
                                                    )
                                                )
                                                ->where($where)
                                                ->where($andwhere)
                                                ->order('k.name ASC');
            
                if($this->_wss_dbg){
                    echo "<br />\n";
                    $sql = $select->__toString();
                    echo "$sql\n"."<br />\n";
                }
                
                $statement = $this->getAdapter()->query($select);
                $result = $statement->fetchAll();
                
                //array_unshift($result, Array('Text' => '', 'Value' => '-1'));
                array_unshift($result, Array('Text' => '', 'Value' => $_REQUEST['ParentId']));
                
                return $result;
                
            }
            else{
                
                //region
                
                $this->_name = 'jos_kladr';
                
                $where = $this->getAdapter()->quoteInto(' `k`.`code` LIKE ? ', '__00000000000');
                         
                $select = $this->getAdapter()   ->select()
                                                ->from(
                                                    array('k' => 'jos_kladr'),
                                                    array(
                                                        'k.name', 'k.code', 'k.index', 'k.socr',
                                                        'Text' => new Zend_Db_Expr('CONCAT_WS(\' \', `k`.`name`, CONCAT_WS(\'\', `k`.`socr`, \'.\'))'), 
                                                        'Value' => 'k.code',
                                                        
                                                    )
                                                )
                                                ->where($where)
                                                ->order('k.name ASC');
            
                if($this->_wss_dbg){
                    echo "<br />\n";
                    $sql = $select->__toString();
                    echo "$sql\n"."<br />\n";
                }
                
                $statement = $this->getAdapter()->query($select);
                $result = $statement->fetchAll();
                
                array_unshift($result, Array('Text' => '', 'Value' => '-1'));
                
                return $result;
            }
             
        }
        
        if($idx && false && 0){//disabled
            
            $this->_name = 'jos_kladr';
            
            $where = $this->getAdapter()->quoteInto(' `k`.`index` = ? ', "$idx");
            
            $select = $this->getAdapter()   ->select()
                                            ->from(
                                                array('k' => 'jos_kladr'),
                                                array(
                                                    'k.name', 'k.code', 'k.index', 'k.socr'
                                                )
                                            )
                                            ->where($where);
        
            if($this->_wss_dbg){
                echo "<br />\n";
                $sql = $select->__toString();
                echo "$sql\n"."<br />\n";
            }
            
            $statement = $this->getAdapter()->query($select);
            $result = $statement->fetchAll();
            
            $idxobj = array_merge($idxobj, $result);
            
            $this->_name = 'jos_kladr_street';
            
            $where = $this->getAdapter()->quoteInto(' `ks`.`index` = ? ', "$idx");
            
            $select = $this->getAdapter()   ->select()
                                            ->from(
                                                array('ks' => 'jos_kladr_street'),
                                                array(
                                                    'ks.name', 'ks.code', 'ks.index', 'ks.socr'
                                                )
                                            )
                                            ->where($where);
        
            if($this->_wss_dbg){
                echo "<br />\n";
                $sql = $select->__toString();
                echo "$sql\n"."<br />\n";
            }
            
            $statement = $this->getAdapter()->query($select);
            $result = $statement->fetchAll();
            
            $idxobj = array_merge($idxobj, $result);
            
        }
        

        
        if($cty){
            
            $this->_name = 'jos_kladr';
            
            $where = $this->getAdapter()->quoteInto(' `k`.`name` LIKE ? ', '%'."$cty".'%');
            
            $select = $this->getAdapter()   ->select()
                                            ->from(
                                                array('k' => 'jos_kladr'),
                                                array(
                                                    'k.name', 'k.code', 'k.index', 'k.socr'
                                                )
                                            )
                                            ->where($where);
        
            if($this->_wss_dbg){
                echo "<br />\n";
                $sql = $select->__toString();
                echo "$sql\n"."<br />\n";
            }
            
            $statement = $this->getAdapter()->query($select);
            $result = $statement->fetchAll();
            
            $ctyobj = array_merge($ctyobj, $result);
            
        }
        

        
        //if($str){
        //if($cty && $str){
        if(count($ctyobj) && $str){
            
            $this->_name = 'jos_kladr_street';
            
            $where = $this->getAdapter()->quoteInto(' `ks`.`name` LIKE ? ', '%'."$str".'%');
            

            
            $select = $this->getAdapter()   ->select()
                                            ->from(
                                                array('ks' => 'jos_kladr_street'),
                                                array(
                                                    'ks.name', 'ks.code', 'ks.index', 'ks.socr'
                                                )
                                            )
                                            ->where($where);

            $wheres = array();
            if(count($ctyobj)){
                foreach ($ctyobj as $city){
                    $ccode = $city['code'];
                    $ccode = substr($ccode, 0 , 6);
                    $where = $this->getAdapter()->quoteInto(' `ks`.`code` LIKE ? ', "$ccode".'%');
                    array_push($wheres, "$where");
                }
                $wheres = implode(' OR ', $wheres);
                $select=$select->where($wheres);
            }
        
            if($this->_wss_dbg){
                echo "<br />\n";
                $sql = $select->__toString();
                echo "$sql\n"."<br />\n";
            }
            
            $statement = $this->getAdapter()->query($select);
            $result = $statement->fetchAll();
            
            $strobj = array_merge($strobj, $result);
            
            foreach ($strobj as $strrow){
                
                $this->_name = 'jos_kladr_doma';
                
                if(empty($strrow['index'])){
                    
                    $where = $this->getAdapter()->quoteInto(' `kd`.`code` LIKE ? ', $strrow['code'].'%');
                    
                    $select = $this->getAdapter()   ->select()
                                                    ->from(
                                                        array('kd' => 'jos_kladr_doma'),
                                                        array(
                                                            'kd.name', 'kd.code', 'kd.index', 'kd.socr'
                                                        )
                                                    )
                                                    ->where($where);
                    
                    if($this->_wss_dbg){
                        echo "<br />\n";
                        $sql = $select->__toString();
                        echo "$sql\n"."<br />\n";
                    }
                    
                    $statement = $this->getAdapter()->query($select);
                    $result = $statement->fetchAll();
                
                    if(!$dom || empty($dom)){
                        $domobj['c'.$strrow['code']] = $result;
                    }
                    else{
                        $domobj['c'.$strrow['code']] = array();
                        foreach ($result as $domrow){
                            $domprenums = $domrow['name'];
                            $domprenums = explode(',', $domprenums);
                            foreach ($domprenums as $domnum){
                                $domnum = trim($domnum);
                                $dommatches=array();
                                if(preg_match('/([^\(]*)?[\(]?(\d+\D*)\-(\d+\D*)/', $domnum, $dommatches)){
                                    $domtype = $dommatches[1];
                                    $dompre = intval($dommatches[2]);
                                    $domafter = intval($dommatches[3]);
                                    
                                    if(intval($dom) >= $dompre && intval($dom) <= $domafter){
                                        if(
                                            (!$domtype || empty($domtype))
                                                ||
                                            (
                                                $domtype && $domtype == 'Н' && (intval($dom)%2)
                                            )
                                                ||
                                            (
                                                $domtype && $domtype == 'Ч' && !(intval($dom)%2)
                                            )
                                        ){

                                            $domrow['name'] = str_replace('Ч(', 'Четн.(', $domrow['name']);
                                            $domrow['name'] = str_replace('Н(', 'НеЧетн.(', $domrow['name']);
                                            array_push($domobj['c'.$strrow['code']], $domrow);
                                        }
                                    }
                                }
                                elseif(preg_match('/(\d+)/', $domnum, $dommatches)){
                                    //$domnum = intval($domnum);
                                    $domnum = intval($dommatches[1]);
                                    if($domnum==intval($dom)){
                                        array_push($domobj['c'.$strrow['code']], $domrow);
                                    }
                                }
                                else{
                                    //pass
                                    
                                }
                                
                            }
                        }
                    }
                
                }
            }
            
        }
        
        
        return array(
            'idxs' => $idxobj,
            'ctys' => $ctyobj,
            'strs' => $strobj,
            'doms' => $domobj, 
        );
        
    }
    //-getKladrResults
    
}

