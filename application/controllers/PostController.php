<?php
/**
 * Created by PhpStorm.
 * User: Aleksandr Taran
 * Date: 17.08.2016
 * Time: 12:33
 */

require_once 'App/Controller/Admin.php';
require_once 'entity/Post/Post.php';

class PostController extends App_Controller_Admin {

    protected $post;

    public function init()
    {
        parent::init();
        $this->post = new Application_Entity_Post_Post();
    }

    public function indexAction()
    {
        $this->view->posts = $this->post->last()->all();
    }

    public function showAction()
    {
        $this->view->post = $this->post->find(intval($this->_request->getParam('id')))->get();
    }

    public function createAction()
    {
        // just show creating form
    }

    public function storeAction()
    {
        if ($this->getRequest()->isPost()) {
            $form = new Application_Form_Post();
            if($form->isValid($_POST)) {
                $this->post->save($this->getRequest()->getParams());
                $this->_helper->getHelper('flashMessenger')->addMessage('Успешно сохранено.');
                $this->_redirect('/post');
            }
            else {
                print_r($form->getErrors()); exit;
                $this->render('create');
            }
        }
    }

    public function editAction()
    {
        $this->view->post = $this->post->find(intval($this->_request->getParam('Id')))->get();
    }

    public function patchAction()
    {
        if($this->post = $this->post->find(intval($this->_request->getParam('Id')))) {
            if ($this->getRequest()->isPost()) {
                $form = new Application_Form_Post();
                if($form->isValid($_POST)) {
                    $this->post->save($this->getRequest()->getParams());
                    $this->_helper->getHelper('flashMessenger')->addMessage('Успешно сохранено.');
                    $this->_redirect('/post');
                }
                else {
                    print_r($form->getErrors()); exit;
                    $this->render('create');
                }
            }
        }
        else {
            // неверный идентификатор редактируемой записи
        }

        $this->post->set($this->getRequest()->getParams());
    }

    public function destroyAction()
    {
        if($this->getRequest()->isXmlHttpRequest()) {
            if(!intval($this->_request->getParam('Id'))) {
                $this->_helper->json(
                    array('msg' => 'no Id',
                        'Id' => $this->_request->getParam('Id'))
                );
                return;
            }

            if($result = $this->post->destroy(intval($this->_request->getParam('Id')))) {
                $this->_helper->json(
                    array('msg' => $result,
                        'Id' => $this->_request->getParam('Id'))
                );
            }
            else{
                $this->_helper->json(
                    array('msg' => 'can\'t destroy',
                        'Id' => $this->_request->getParam('Id'))
                );
                return;
            }
        }
        else {
            throw new Zend_Http_Exception('Not Found', 404);
        }
    }
}