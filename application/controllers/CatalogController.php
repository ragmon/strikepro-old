<?php
/**
 * CatalogController
 * 
 * @author Alexander Taran
 * @version 0.1
 */
require_once 'App/Controller/Action.php';
require_once 'entity/CatalogObject/AbstractCatalogObjectFactory.php';
require_once 'entity/Goods/GoodsFactory.php';

class CatalogController extends App_Controller_Action {

    public $_model;
    public $_modelGoods;
    private $catalog_object;

    public function init()
    {
        parent::init();
        $this->_model = new Application_Model_CatalogObject();
        $this->_modelGoods = new Application_Model_Goods();
    }
    
    public function indexAction()
    {
        $this->_helper->_layout->setLayout('site/bootstrap-layout');

        $this->view->wss_art = strval($this->_request->getParam('art'));
        $link = strval($this->_getParam('catLink'));

        if(!$link) {
            $link = 'strikepro';
        }

        if (!is_object($this->catalog_object = AbstractCatalogObjectFactory::getCatalogObjectFactory()->getCatalogObjectByLink($link))) {
            throw new Zend_Http_Exception('Запрашиваемый ресурс не найден', 404);
        }
        else {
            if(is_a($this->catalog_object, 'CatalogObjectTypeDirectory')) {
                $this->catalog_object->withChilds();
                $goods = $this->_modelGoods->getGoodsListByCat(1198);

                $this->catalog_object->child_objects = array_merge($this->catalog_object->child_objects, $goods);
                uasort($this->catalog_object->child_objects, array(&$this, 'alphabeticSort'));
            }
        }

        $this->view->path = $this->_model->getPath($this->catalog_object->cat_sub_of);
        $this->view->catalog_object = $this->catalog_object;
        $this->_helper->viewRenderer($this->catalog_object->getPattern());
    }

    public function alphabeticSort($f1,$f2)
    {
        if (is_a($f1, 'CatalogObjectProductDirectory')) $left = $f1->cat_name;
        else if (is_a($f1, 'Goods')) $left = $f1->article_name;
        else $left = $f1->article_name;

        if (is_a($f2, 'CatalogObjectProductDirectory')) $right = $f2->cat_name;
        else if (is_a($f2, 'Goods')) $right = $f2->article_name;
        else $right = $f2->article_name;

        if($left < $right) return -1;
        elseif($left > $right) return 1;
        else return 0;
    }


    // ---------------------------------------------------------------------------------------------------------------

    public function articleAction()
    {
        $this->_helper->_layout->setLayout('site/bootstrap-layout');

        $this->view->wss_art = strval($this->_request->getParam('art'));
        $link = strval($this->_getParam('catLink'));

        if (!is_object($this->goods_object = GoodsFactory::getInstance($link)->get())) {
            throw new Zend_Http_Exception('Запрашиваемый ресурс не найден', 404);
        }

        $this->view->path = $this->_model->getPath($this->goods_object->article_cat);
        $this->view->goods_object = $this->goods_object;
        $this->_helper->viewRenderer($this->goods_object->getPattern());
    }
}
