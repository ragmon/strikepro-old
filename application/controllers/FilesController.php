<?php
/**
 * Created by PhpStorm.
 * User: Aleksandr Taran
 * Date: 15.11.2016
 * Time: 12:44
 */

require_once 'entity/FileManager/FileManager.php';

class FilesController extends App_Controller_Admin {

    private  $_FM;

    public function init()
    {
        parent::init();
        $this->_helper->_layout->setLayout('fm/layout');
        $this->_FM = new Application_Entity_FileManager_FileManager();
    }

    public function browseAction()
    {
        $this->view->files = $this->_FM->getFiles();
        $this->view->url = $this->_FM->getURL();
    }

    public function storeAction()
    {
        require_once 'plugins/simple-ajax-loader/Uploader.php';
        $config = new Zend_Config_Ini('configs/application.ini', 'production');
        $uploadDir = $config->basepath . $config->filemanager->dir;
        $Upload = new FileUpload('uploadfile');
        $Upload->newFileName = md5(microtime() . $Upload->getFileName()) . '.' . $Upload->getExtension();
        $result = $Upload->handleUpload($uploadDir, array('png', 'jpeg', 'jpg'));

        if($result) {
            $this->_helper->json(array('success' => true,
                'msg' => 'файл загружен',
                'output' => $config->baseurl . $config->filemanager->url . '/' .  $Upload->newFileName
            ));
        }
    }

    public function deleteAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        if($this->getRequest()->isXmlHttpRequest()) {

            $file = $this->getRequest()->getParam('file');

            $config = new Zend_Config_Ini('configs/application.ini', 'production');
            $directory = $config->basepath . $config->filemanager->dir;

            if(unlink($directory. '/' . $file)) {
                $this->_helper->json(array('success' => true,
                    'message' => 'файл удален',
                    'file' => $file
                ));
            }
            else {
                $this->_helper->json(array('success' => false,
                    'message' => 'файл не удален',
                    'file' => $file
                ));
            }
        }
    }

    public function progressAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        if($this->getRequest()->isXmlHttpRequest()) {

            if (isset($_SERVER['HTTP_ORIGIN'])) {
                header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
                header('Access-Control-Allow-Credentials: true');
                header('Access-Control-Max-Age: 86400');    // cache for 1 day
            }

            if (isset($_REQUEST['progresskey'])) {
                $status = apc_fetch('upload_'.$_REQUEST['progresskey']);
            }
            else {
                exit($this->_helper->json(array('success' => false)));
            }

            $pct = 0;
            $size = 0;

            if (is_array($status)) {
                if (array_key_exists('total', $status) && array_key_exists('current', $status)) {

                    if ($status['total'] > 0) {

                        $pct = round(($status['current'] / $status['total']) * 100);
                        $size = round($status['total'] / 1024);
                    }
                }
            }

            $this->_helper->json(
                array(  'success' => true,
                    'pct' => $pct,
                    'size' => $size
                ));
        }
    }
}