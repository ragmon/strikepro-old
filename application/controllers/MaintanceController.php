<?php
/**
 * Created by PhpStorm.
 * User: Aleksandr Taran
 * Date: 31.08.2016
 * Time: 10:13
 */

class MaintanceController extends App_Controller_Action
{
    public function init()
    {

    }

    public function preDispatch()
    {
        $this->_helper->layout()->disableLayout();
    }

    public function indexAction()
    {
        return true;
    }
}