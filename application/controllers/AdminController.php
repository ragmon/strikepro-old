<?php
/**
 * Created by PhpStorm.
 * User: Aleksandr Taran
 * Date: 17.08.2016
 * Time: 12:33
 */

require_once 'App/Controller/Admin.php';

class AdminController extends App_Controller_Admin {

    public function init()
    {
        parent::init();
    }

    /**
     * The default action - show the home page
     */
    public function indexAction()
    {

    }

}
